package view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SearchView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android_kotlin.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.entity.ApartmentEntity
import model.entity.UserEntity
import model.utils.Connectivity
import presenter.ListApartmentAdapter
import presenter.ListApartmentPresenter
import utils.CachingImage
import utils.LRUCacheManager
import utils.LocalStorageUser
import utils.UserData
import java.util.Locale

class ListApartmentActivity : AppCompatActivity() {

    private lateinit var listHousePresenter : ListApartmentPresenter
    private lateinit var listHouseAdapter: ListApartmentAdapter
    private lateinit var houseRecyclerView : RecyclerView
    private lateinit var houseArrayList : ArrayList<ApartmentEntity>
    private lateinit var tempHouseArrayList : ArrayList<ApartmentEntity>
    private lateinit var searchView: SearchView
    private lateinit var roomView : TextView
    private lateinit var priceView : TextView
    private lateinit var connectionChecker : Connectivity
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var user: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout
    private var roomavg = 0
    private var priceavg = 0
    private var count = 0
    private var maxMemory = 0
    private var cacheSize = 0
    private var cacheManager = LRUCacheManager()

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_apartment)

        lifecycleScope.launch(Dispatchers.Main) {

            val keyUser = UserData.USER_LOGIN
            val user = loadUserFromSharedPreferences(keyUser)
            if (user == null) {
                showError("an error occurred, NO DATA")
            } else {
                menu(keyUser, user)
                houseRecyclerView = findViewById(R.id.houseList)
                houseRecyclerView.layoutManager = LinearLayoutManager(this@ListApartmentActivity)
                houseRecyclerView.setHasFixedSize(true)
                searchView = findViewById(R.id.searchViewHouses)
                roomView = findViewById(R.id.roomavg)
                priceView = findViewById(R.id.priceavg)
                houseArrayList = ArrayList()
                tempHouseArrayList = ArrayList()
                listHouseAdapter = ListApartmentAdapter(houseArrayList)
                listHousePresenter = ListApartmentPresenter()
                houseRecyclerView.adapter = listHouseAdapter

                connectionChecker = Connectivity(applicationContext)
                maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
                cacheSize = maxMemory / 10
                cacheManager.setCacheSize(cacheSize)

                listHouseAdapter.onItemClick = {
                    val intent = Intent(this@ListApartmentActivity, DetailApartment::class.java)
                    intent.putExtra("detailed_apartment", it)
                    intent.putExtra("detailed_apartment_name", it.name)
                    startActivity(intent)
                }

                lifecycleScope.launch(Dispatchers.IO) {
                    if (cacheManager.size() > 0 && !connectionChecker.isNetworkAvailable()) {
                        getCacheData()
                    } else {
                        var startTime = System.currentTimeMillis()
                        var timeout = false
                        while (!connectionChecker.isNetworkAvailable()) {
                            var actualTime = System.currentTimeMillis()
                            var timeElapsed = (actualTime - startTime) / 1000
                            if ((timeElapsed > 5) && !timeout){
                                runOnUiThread{
                                    printLoad()
                                }
                                timeout = true
                            }
                        }
                        getNetworkData()
                        for (apartment in houseArrayList) {
                            cacheManager.put(apartment.name, apartment)
                        }
                    }
                }

                searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String?): Boolean {
                        searchView.clearFocus()
                        return true
                    }
                    override fun onQueryTextChange(newLocality: String?): Boolean {

                        roomavg = 0
                        priceavg = 0
                        count = 0

                        tempHouseArrayList.clear()
                        val searchText = newLocality!!.lowercase(Locale.getDefault())
                        if (searchText.isNotEmpty()){
                            houseArrayList.forEach {
                                if (it.neighborhood.lowercase(Locale.getDefault()).contains(searchText)) {
                                    tempHouseArrayList.add(it)
                                    roomavg += it.roomsNumber
                                    priceavg += it.rentPrice.toInt()
                                    count += 1
                                }
                            }
                            if (tempHouseArrayList.isNotEmpty()){
                                roomavg /= count
                                priceavg /= count
                            }
                            roomView.text = roomavg.toString()
                            priceView.text = priceavg.toString()
                            houseRecyclerView.adapter!!.notifyDataSetChanged()

                        }
                        else {
                            tempHouseArrayList.clear()
                            tempHouseArrayList.addAll(houseArrayList)
                            var avgsList: ArrayList<Int> = listHousePresenter.setOriginalAvgs(houseArrayList)
                            roomView.text = avgsList[0].toString()
                            priceView.text = avgsList[1].toString()
                            houseRecyclerView.adapter!!.notifyDataSetChanged()
                        }

                        listHouseAdapter.setFilteredList(tempHouseArrayList)

                        return false
                    }
                })
            }
        }
    }

    private fun menu(keyUser: String, users: UserEntity) {
        drawerLayout =  findViewById(R.id.drawer_layout_list_apartment)
        menu =  findViewById(R.id.button_menu)
        home =  findViewById(R.id.home)
        user =  findViewById(R.id.users)
        apartmentOffers =  findViewById(R.id.apartments_offer)
        dashboard =  findViewById(R.id.dashboard)
        profile =  findViewById(R.id.profile)
        publish =  findViewById(R.id.publish)
        logout =  findViewById(R.id.logout)

        val foto = findViewById<ImageView>(R.id.nav_header_image)
        val nombre = findViewById<TextView>(R.id.nav_header_name_user)
        val email = findViewById<TextView>(R.id.nav_header_email)

        CachingImage.downloadImage(foto, users.image)
        nombre.text =  users.full_name
        email.text =  users.email
        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            recreate()
        }

        user.setOnClickListener {
            redirectActivity(this, ListUser::class.java)
        }
        apartmentOffers.setOnClickListener {
            redirectActivity(this, ListApartmentOffers::class.java)
        }
        profile.setOnClickListener { redirectActivity(this, Profile::class.java) }
        publish.setOnClickListener {
            redirectActivity(this, PublishApartmentLocation::class.java)
        }
        dashboard.setOnClickListener {
            redirectActivity(this, Dashboard::class.java)
        }
        logout.setOnClickListener {
            val d = LocalStorageUser.deleteUser(this, keyUser)
            Log.d("Valores", "Logout exitoso")
            redirectActivity(this, SignIn::class.java)
        }

    }

    private suspend fun loadUserFromSharedPreferences(keyUser: String): UserEntity? {
        return withContext(Dispatchers.Default) {
            return@withContext LocalStorageUser.getUserEntityFromSharedPreferences(this@ListApartmentActivity, keyUser)
        }
    }

    private fun openDrawer(drawerLayout: DrawerLayout)
    {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout)
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity> )
    {
        val next: Intent =  Intent(activity, secondActivity)
        next.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause()
    {
        super.onPause()
        closeDrawer(drawerLayout)
    }

    private suspend fun getNetworkData() {
        houseArrayList = listHousePresenter.getAllApartments()!!
        updateUI()
    }

    private fun getCacheData() {
        houseArrayList = cacheManager.getAll()
        updateUI()
    }

    private fun updateUI(){
        tempHouseArrayList.addAll(houseArrayList)
        var avgsList: ArrayList<Int> = listHousePresenter.setOriginalAvgs(houseArrayList)
        runOnUiThread {
            roomView.text = avgsList[0].toString()
            priceView.text = avgsList[1].toString()
            listHouseAdapter.notifyDataSetChanged()
            listHouseAdapter.setFilteredList(houseArrayList)
        }
    }

    private fun printLoad(){
        Toast.makeText(this, "The operation is taking more than expected. Check your internet connection", Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed()
    {
        Toast.makeText(this, "You cannot go back", Toast.LENGTH_SHORT).show()
    }

     private fun showError(message: String) {
        runOnUiThread {

            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }
    }

}