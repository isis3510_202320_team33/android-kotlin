package view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputFilter
import android.text.InputType
import android.text.TextWatcher
import android.widget.Button
import android.widget.EditText
import com.example.android_kotlin.R
import android.widget.ImageButton
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import java.util.*
import java.text.NumberFormat

class PublishApartmentInformation : AppCompatActivity() {

    private lateinit var rentPrice: EditText
    private lateinit var stratum: Spinner
    private lateinit var area: EditText
    private lateinit var apartmentFloor: Spinner
    private lateinit var roomsNumber: Spinner
    private lateinit var roomArea: EditText
    private lateinit var bathroomsNumber: Spinner
    private lateinit var sharePreference: SharedPreferences

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.publish_apartment_information)

        rentPrice = findViewById(R.id.rent_price)
        stratum = findViewById(R.id.stratum)
        area = findViewById(R.id.area)
        apartmentFloor = findViewById(R.id.apartment_floor)
        roomsNumber = findViewById(R.id.rooms_number)
        roomArea = findViewById(R.id.room_area)
        bathroomsNumber = findViewById(R.id.bathrooms_number)

        validationInputs()

        val continueButton = findViewById<Button>(R.id.button_dos)
        val sectionInformation =  findViewById<TextView>(R.id.information)
        drawBorderBlue(sectionInformation)

        sharePreference = getSharedPreferences("DATA_HOUSE", Context.MODE_PRIVATE)
        val getRentPrice = sharePreference.getString("RENT_PRICE", "")
        val getStratum = sharePreference.getString("STRATUM", "")
        val getArea =  sharePreference.getString("AREA", "")

        val getApartmentFloor = sharePreference.getString("APARTMENT_FLOOR", "")
        val getRoomsNumber =  sharePreference.getString("ROOMS_NUMBER", "")
        val getRoomArea =  sharePreference.getString("ROOM_AREA", "")
        val getBathroomsNumber = sharePreference.getString("BATHROOMS_NUMBER", "")

        if (getRentPrice != "" && getStratum != "" && getArea != "" &&
            getApartmentFloor != "" && getRoomsNumber != "" && getRoomArea != "" &&
            getBathroomsNumber != "") {

            rentPrice.setText(getRentPrice)
            val stratumPosition = getPositionAttribute(stratum, getStratum!!)
            val apartmentFloorPosition = getPositionAttribute(apartmentFloor, getApartmentFloor!!)
            val roomsNumberPosition = getPositionAttribute(roomsNumber, getRoomsNumber!!)
            val bathroomsNumberPosition = getPositionAttribute(bathroomsNumber, getBathroomsNumber!!)
            stratum.setSelection(stratumPosition)
            area.setText(getArea)
            apartmentFloor.setSelection(apartmentFloorPosition)
            roomsNumber.setSelection(roomsNumberPosition)
            roomArea.setText(getRoomArea)
            bathroomsNumber.setSelection(bathroomsNumberPosition)
        }
        continueButton.setOnClickListener {

                validateAndProcessInput(rentPrice, stratum, area, apartmentFloor,
                    roomsNumber, roomArea, bathroomsNumber)

        }

        back_to()
    }

    private fun drawBorderBlue(location: TextView)
    {
        val drawable = GradientDrawable()
        drawable.setColor(Color.parseColor("#2E5EAA"))
        val cornerRadius = resources.getDimensionPixelSize(R.dimen.border_section)
        drawable.cornerRadius = cornerRadius.toFloat()
        location.background = drawable
    }


    fun validateAndProcessInput( rentPrice: EditText, stratum: Spinner, area: EditText, apartmentFloor: Spinner,
                                 roomsNumber: Spinner, roomArea: EditText, bathroomsNumber: Spinner)
    {
        val priceS = rentPrice.text.toString()
        val stratumS = stratum.selectedItem.toString()
        val areaS = area.text.toString()

        val apartmentFloorS = apartmentFloor.selectedItem.toString()
        val roomsNumberS = roomsNumber.selectedItem.toString()
        val roomAreaS = roomArea.text.toString()
        val bathroomsNumberS = bathroomsNumber.selectedItem.toString()

        val areaPattern = Regex("^\\d{1,2}(\\.\\d{2})?$")
        val roomAreaPattern = Regex("^\\d{1,2}(\\.\\d{2})?$")
        val price = convertPricetoInt(priceS)
        if (price == 0) {
            showError("Please complete field price.")
        }
        else if (stratumS == "Select a stratum" )
        {
            showError("Please complete field stratum.")
        }
        else if (areaS.isEmpty() )
        {
            showError("Please complete field area.")
        }
        else if (apartmentFloorS == "Select an apartment floor")
        {
            showError("Please complete field apartment floor.")
        }
        else if (roomsNumberS == "Select a rooms number")
        {
            showError("Please complete field rooms number.")
        }
        else if (roomAreaS.isEmpty() )
        {
            showError("Please complete field room area.")
        }
        else if (bathroomsNumberS == "Select a bathrooms number" )
        {
            showError("Please complete field bathrooms number.")
        }
        else if (!areaPattern.matches(areaS) || !roomAreaPattern.matches(roomAreaS)  ) {
            showError("Formato inválido. Debe ser 10.00 o 16.23")
        }
        else {
            nextPage(priceS, stratumS, areaS, apartmentFloorS, roomsNumberS,
                roomAreaS, bathroomsNumberS)
        }
    }

    private fun showError(message: String) {
            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
    }

    private fun nextPage(rentPrice: String, stratum: String, area: String, apartmentFloor: String,
                         roomsNumber: String, roomArea: String, bathroomsNumber: String)
    {

        val editor = sharePreference.edit()
        editor.putString("RENT_PRICE", rentPrice)
        editor.putString("STRATUM", stratum)
        editor.putString("AREA", area)
        editor.putString("APARTMENT_FLOOR", apartmentFloor)
        editor.putString("ROOMS_NUMBER", roomsNumber)
        editor.putString("ROOM_AREA", roomArea)
        editor.putString("BATHROOMS_NUMBER", bathroomsNumber)
        editor.apply()
        val next: Intent =  Intent(this, PublishApartmentServices:: class.java)
        startActivity(next)
    }

    private fun back_to()
    {
        val continues_location: ImageButton =  findViewById(R.id.go_back_l)
        continues_location.setOnClickListener {

            val next: Intent =  Intent(this, PublishApartmentLocation:: class.java)
            startActivity(next)
        }
    }

    private fun validationInputs() {
        rentPrice.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        val maxLengthRentPrice = 9
        val inputFilterLengthRentPrice = InputFilter.LengthFilter(maxLengthRentPrice)
        rentPrice.filters = arrayOf(inputFilterLengthRentPrice)

        rentPrice.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
            override fun afterTextChanged(s: Editable?) {
                if (!s.isNullOrBlank()) {
                    val originalText = s.toString()
                    val cleanString = originalText.replace("[^\\d]".toRegex(), "")
                    val parsed = cleanString.toLongOrNull()
                    val formatted = NumberFormat.getNumberInstance(Locale("es", "CO")).format(parsed ?: 0)
                    if (formatted != originalText) {
                        rentPrice.setText(formatted)
                        rentPrice.setSelection(rentPrice.text.length)
                    }
                }
            }
        })


        area.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        val maxLengthArea = 5
        val inputFilterLengthArea = InputFilter.LengthFilter(maxLengthArea)
        area.filters = arrayOf(inputFilterLengthArea)

        roomArea.inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
        val maxLengthRoomArea = 5
        val inputFilterLengthRoomArea = InputFilter.LengthFilter(maxLengthRoomArea)
        roomArea.filters = arrayOf(inputFilterLengthRoomArea)
    }


    private fun convertPricetoInt(price: String): Int {
        val cleanPrice = price.replace("[^\\d]".toRegex(), "")
        return if (cleanPrice.isNotEmpty()) {
            cleanPrice.toInt()
        } else {
            0
        }
    }

    private fun getPositionAttribute(spinner: Spinner, itemName: String): Int {
        val adapter = spinner.adapter
        var position = 0
        for (i in 0 until adapter.count) {
            if (adapter.getItem(i).toString() == itemName) {
                position = i
            }
        }
        return position
    }

}
