package view

import FirebaseDataBaseSingleton
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android_kotlin.R
import contract.ApartmentContract
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.FirebaseAuthSingleton
import model.FirebaseStorageSingleton
import model.interactor.ApartmentInteractor
import model.repository.IApi.ApiDescriptions
import model.utils.Connectivity
import presenter.ApartmentPresenter
import presenter.PopularDescriptionsAdapter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import utils.CacheManagerDescriptions
import utils.LocalStorageUser
import utils.TinyDB
import utils.UserData


class PublishApartmentPhotos : AppCompatActivity(), ApartmentContract.viewPhoto  {


    private lateinit var description: EditText
    private lateinit var connectionChecker : Connectivity
    private val takenImages = mutableListOf<Bitmap>()
    private lateinit var apartmentPresenter: ApartmentPresenter
    private lateinit var textNumberPhotos: TextView
    private lateinit var continueButton: Button
    private lateinit var dialog: Dialog
    private lateinit var sharePreference: SharedPreferences

    private var progressDialog: AlertDialog? = null
    @SuppressLint("MissingInflatedId", "SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.publish_apartment_photos)
        FirebaseDataBaseSingleton.init(this)
        FirebaseStorageSingleton.getInstance()
        FirebaseAuthSingleton.getInstance()

        connectionChecker = Connectivity(applicationContext)
        val cacheManager = CacheManagerDescriptions.getInstance()

        val recyclerView = findViewById<RecyclerView>(R.id.populardescriptions)

        val popularDescriptionsText= findViewById<TextView>(R.id.descriptions)
        val sliderContainer = findViewById<LinearLayout>(R.id.slidercontainer)
        val retrofit = Retrofit.Builder()
            .baseUrl("http://34.123.214.132:8000/senehouse/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val apiService = retrofit.create(ApiDescriptions::class.java)

        fun updateUI(descriptions: List<String>) {
            val adapter = PopularDescriptionsAdapter(descriptions)
            recyclerView.adapter = adapter
            val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            recyclerView.layoutManager = layoutManager
        }

        var tinyDB = TinyDB(applicationContext)
        fun onButtonClick() {
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val descriptions = apiService.getBestDescriptions()
                    cacheManager.saveToCache("descriptions", descriptions)

                    tinyDB.putListString("descripciones",
                        descriptions as java.util.ArrayList<String>?
                    );

                    withContext(Dispatchers.Main) {
                        updateUI(descriptions)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        fun isNetworkAvailable(): Boolean {
            return connectionChecker.isNetworkAvailable()
        }
        popularDescriptionsText.setOnClickListener {
            if (sliderContainer.visibility == View.VISIBLE) {
                sliderContainer.visibility = View.GONE
            } else {
                sliderContainer.visibility = View.VISIBLE
                if (isNetworkAvailable()) {
                    onButtonClick()
                } else {
                    val cachedData = cacheManager.getFromCache("Descriptions")
                    if (cachedData != null){
                        updateUI(cachedData)
                        Toast.makeText(this, "No internet connection. Showing cached data.", Toast.LENGTH_SHORT).show()
                    }
                    else {
                        var data : ArrayList<String>? = tinyDB.getListString("descripciones")
                        if(data == null){
                            Toast.makeText(this, "No internet connection and no cached data.", Toast.LENGTH_SHORT).show()
                        }
                        else{
                            updateUI(data.toList())
                            Toast.makeText(this, "No internet connection. Showing cached data.", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }
        }

        val sectionPhotos =  findViewById<TextView>(R.id.photos)
        drawBorderBlue(sectionPhotos)
        sharePreference = getSharedPreferences("DATA_HOUSE", Context.MODE_PRIVATE)
        textNumberPhotos = findViewById(R.id.text_number_photos)

        val addPhotosButton = findViewById<Button>(R.id.add_photo)
        addPhotosButton.setOnClickListener {
            startForResult.launch(Intent(MediaStore.ACTION_IMAGE_CAPTURE))
        }

        continueButton = findViewById(R.id.button_cuatro)
        description =  findViewById(R.id.description)


        continueButton.setOnClickListener {

            runOnUiThread {
                validateAndProcessInput()
            }
        }

        back_to()
    }

    private val startForResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
            if (result.resultCode == Activity.RESULT_OK) {
                val intent = result.data
                val imageBitmap = intent?.extras?.get("data") as Bitmap
                if (takenImages.size < 20) {
                    takenImages.add(imageBitmap)
                    textNumberPhotos.text = takenImages.size.toString()
                } else {
                    showError("Se alcanzó el máximo de 20 imágenes.")
                }
            }
        }

    private fun drawBorderBlue(location: TextView)
    {
        val drawable = GradientDrawable()
        drawable.setColor(Color.parseColor("#2E5EAA"))
        val cornerRadius = resources.getDimensionPixelSize(R.dimen.border_section)
        drawable.cornerRadius = cornerRadius.toFloat()
        location.background = drawable
    }

    private fun validateAndProcessInput() {
        val descriptionD = description.text.toString()
        if (descriptionD.isEmpty()) {
            showError("Please provide a brief description of the property.")
        } else if (takenImages.size < 2) {
            showError("You must take at least 2 images.")
        } else {

            sharePreference = getSharedPreferences("DATA_HOUSE", Context.MODE_PRIVATE)

            val getName = sharePreference.getString("NAME", "")
            val getCity = sharePreference.getString("CITY", "")
            val getNeighborhood =  sharePreference.getString("NEIGHBORHOOD", "")
            val getAddress =  sharePreference.getString("ADDRESS", "")
            val getHousingType = sharePreference.getString("HOUSING_TYPE", "")
            val getLatitude = sharePreference.getString("LATITUDE", "")
            val getLongitude = sharePreference.getString("LONGITUDE", "")
            val getRentPrice = sharePreference.getString("RENT_PRICE", "")
            val getStratum = sharePreference.getString("STRATUM", "")
            val getArea =  sharePreference.getString("AREA", "")
            val getApartmentFloor = sharePreference.getString("APARTMENT_FLOOR", "")
            val getRoomsNumber =  sharePreference.getString("ROOMS_NUMBER", "")
            val getRoomArea =  sharePreference.getString("ROOM_AREA", "")
            val getBathroomsNumber = sharePreference.getString("BATHROOMS_NUMBER", "")
            val getLaundryArea = sharePreference.getBoolean("LAUNDRY_AREA", false)
            val getInternet = sharePreference.getBoolean("INTERNET", false)
            val getTv =  sharePreference.getBoolean("TV", false)
            val getFurnished =  sharePreference.getBoolean("FURNISHED", false)
            val getElevator = sharePreference.getBoolean("ELEVATOR", false)
            val getSmoke =  sharePreference.getBoolean("SMOKE", false)
            val getVape =  sharePreference.getBoolean("VAPE", false)
            val getPets = sharePreference.getBoolean("PETS", false)
            val getGym = sharePreference.getBoolean("GYM", false)
            val getSupermarket = sharePreference.getBoolean("SUPERMARKET", false)
            val getReception = sharePreference.getBoolean("RECEPTION", false)

            val keyUser = UserData.USER_LOGIN
            val user = LocalStorageUser.getUserEntityFromSharedPreferences(this, keyUser)
            if (user == null)
            {
                showError("an error occurred")
            }

            else
            {
                showProgressDialog()
                lifecycleScope.launch(Dispatchers.IO) {
                    val interactor = ApartmentInteractor()
                    apartmentPresenter = ApartmentPresenter(this@PublishApartmentPhotos, interactor)
                    apartmentPresenter.paramsValidation(
                        getName!!, getCity!!, getNeighborhood!!, getAddress!!, getHousingType!!, getLatitude!!, getLongitude!!,
                        getRentPrice!!, getStratum!!, getArea!!, getApartmentFloor!!, getRoomsNumber!!,
                        getRoomArea!!, getBathroomsNumber!!, getLaundryArea, getInternet,  getTv, getFurnished,
                        getElevator, getSmoke, getVape, getPets, getGym, getSupermarket, getReception,
                        descriptionD, takenImages, (user!!.id) )

                }
            }

        }

    }


    override fun nextPage()
    {
        hideProgressDialog()
        clearDataHouse(this)
        val next =  Intent(this, ListApartmentActivity :: class.java)
        startActivity(next)
    }



    override fun showError(message: String)
    {
        runOnUiThread {
            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }

    }

    override fun back_to()
    {
        val continues_services: ImageButton =  findViewById(R.id.go_back_s)
        continues_services.setOnClickListener {
            val next =  Intent(this, PublishApartmentServices:: class.java)
            startActivity(next)
        }
    }


    private fun clearDataHouse(context: Context)
    {
        val sharedPreferences = context.getSharedPreferences("DATA_HOUSE", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        val keysToDelete = listOf(
            "NAME", "CITY", "NEIGHBORHOOD", "ADDRESS", "HOUSING_TYPE",
            "LATITUDE", "LONGITUDE", "RENT_PRICE", "STRATUM", "AREA",
            "APARTMENT_FLOOR", "ROOMS_NUMBER", "ROOM_AREA", "BATHROOMS_NUMBER",
            "LAUNDRY_AREA", "INTERNET", "TV", "FURNISHED", "ELEVATOR",
            "SMOKE", "VAPE", "PETS"
        )

        for (key in keysToDelete) {
            editor.remove(key)
        }
        editor.apply()
    }

    override fun showConnection()
    {
        runOnUiThread {
            hideProgressDialog()
            dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet_apartment)
            dialog.setCancelable(false)

            val retryButton = dialog.findViewById<Button>(R.id.btnRetry)
            retryButton.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    fun redirectToHome(view: View) {

        val intent = Intent(this, Discover::class.java)
        startActivity(intent)
    }

    private fun showProgressDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setView(layoutInflater.inflate(R.layout.progress_dialog, null))
        builder.setCancelable(false)
        progressDialog = builder.create()
        progressDialog?.show()
    }

    private fun hideProgressDialog() {

        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

}