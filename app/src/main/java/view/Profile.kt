package view

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.icu.util.TimeUnit
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import androidx.work.WorkManager
import com.example.android_kotlin.R
import kotlinx.coroutines.withContext
import contract.ProfileContract
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch
import model.entity.UserEntity
import model.interactor.ProfileInteractor
import model.interactor.UserInteractor
import presenter.DashboardPresenter
import presenter.ProfilePresenter
import presenter.UserPresenter
import utils.CachingImage
import utils.LocalStorageUser
import utils.UserData

class Profile : AppCompatActivity(), ProfileContract.view {


    private lateinit var profilePresenter: ProfilePresenter
    private lateinit var coroutineScope: CoroutineScope
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var user: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout

    private lateinit var dialog: Dialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profile)


        lifecycleScope.launch( Dispatchers.Main)
        {
            val keyUser  = UserData.USER_LOGIN
            val user = loadUserFromSharedPreferences(keyUser)
            if (user == null)
            {
                showError("an error occurred, NO DATA")
            }
            else{
                menu(keyUser, user)
                getProfile(user.email, user.password)
            }
        }

    }

    private suspend fun loadUserFromSharedPreferences(keyUser: String): UserEntity? {
        return withContext(Dispatchers.Default) {
            return@withContext LocalStorageUser.getUserEntityFromSharedPreferences(this@Profile, keyUser)
        }
    }

    private fun menu(keyUser: String, users: UserEntity)
    {
        drawerLayout =  findViewById(R.id.drawer_layout_profile)
        menu =  findViewById(R.id.button_menu)
        home =  findViewById(R.id.home)
        user =  findViewById(R.id.users)
        apartmentOffers =  findViewById(R.id.apartments_offer)
        dashboard =  findViewById(R.id.dashboard)
        profile =  findViewById(R.id.profile)
        publish =  findViewById(R.id.publish)
        logout =  findViewById(R.id.logout)

        val foto = findViewById<ImageView>(R.id.nav_header_image)
        val nombre = findViewById<TextView>(R.id.nav_header_name_user)
        val email = findViewById<TextView>(R.id.nav_header_email)

        CachingImage.downloadImage(foto, users.image)
        nombre.text =  users.full_name
        email.text =  users.email

        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            redirectActivity(this, ListApartmentActivity::class.java)
        }
        user.setOnClickListener {
            redirectActivity(this, ListUser::class.java)
        }
        apartmentOffers.setOnClickListener {
            redirectActivity(this, ListApartmentOffers::class.java)
        }
        profile.setOnClickListener { recreate() }
        publish.setOnClickListener {
            redirectActivity(this, PublishApartmentLocation::class.java)
        }
        dashboard.setOnClickListener {
            redirectActivity(this, Dashboard::class.java)
        }
        logout.setOnClickListener {
            val d = LocalStorageUser.deleteUser(this, keyUser)
            Log.d("Valores", "Logout exitoso")
            redirectActivity(this, SignIn::class.java)
        }

    }

    private fun openDrawer(drawerLayout: DrawerLayout)
    {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout)
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }
    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity> )
    {
        val next: Intent =  Intent(activity, secondActivity)
        next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause()
    {
        super.onPause()
        closeDrawer(drawerLayout)
    }

    private suspend fun getProfile(email: String, password: String)
    {
        withContext(Dispatchers.IO) {
            val interactor =  ProfileInteractor()
            profilePresenter = ProfilePresenter(this@Profile, interactor)
            profilePresenter.getProfile(email, password)
        }
    }

    override fun insertValues(user: UserEntity) {

        runOnUiThread {

            val imageValue = findViewById<ImageView>(R.id.profile_image)
            CachingImage.downloadImage(imageValue, user.image)
            val rolValue = findViewById<TextView>(R.id.profile_rol_value)
            rolValue.text = user.rol
            val nameValue = findViewById<TextView>(R.id.profile_name)
            nameValue.text = user.full_name
            val ratingValue = findViewById<TextView>(R.id.profile_rating_value)
            ratingValue.text = (user.stars).toString()

            val tablePersonalInformation = findViewById<TableLayout>(R.id.table_personal_information)
            val row1 = TableRow(this)
            val emailTextView = createTextView("Email", 1f)
            val emailValueTextView = createTextView(user.email, 1f)
            row1.addView(emailTextView)
            row1.addView(emailValueTextView)

            val row2 = TableRow(this)
            val passwordTextView = createTextView("Password", 1f)
            val passwordValueTextView = createTextView("**********", 1f)
            row2.addView(passwordTextView)
            row2.addView(passwordValueTextView)

            val row3 = TableRow(this)
            val cityTextView = createTextView("City", 1f)
            val cityValueTextView = createTextView(user.city, 1f)
            row3.addView(cityTextView)
            row3.addView(cityValueTextView)

            val row4 = TableRow(this)
            val localityTextView = createTextView("Locality", 1f)
            val localityValueTextView = createTextView(user.locality, 1f)
            row4.addView(localityTextView)
            row4.addView(localityValueTextView)

            val row5 = TableRow(this)
            val ageTextView = createTextView("Age", 1f)
            val ageValueTextView = createTextView((user.age).toString(), 1f)
            row5.addView(ageTextView)
            row5.addView(ageValueTextView)

            val row6 = TableRow(this)
            val genderTextView = createTextView("Gender", 1f)
            val genderValueTextView = createTextView(user.gender, 1f)
            row6.addView(genderTextView)
            row6.addView(genderValueTextView)

            val row7 = TableRow(this)
            val phoneTextView = createTextView("Phone", 1f)
            val phoneValueTextView = createTextView((user.phone).toString(), 1f)
            row7.addView(phoneTextView)
            row7.addView(phoneValueTextView)

            val row8 = TableRow(this)
            val personalityTextView = createTextView("Personality", 1f)
            val personality = checkString(user.personality)
            val personalityValueTextView = createTextView(personality, 1f)
            row8.addView(personalityTextView)
            row8.addView(personalityValueTextView)

            tablePersonalInformation.addView(row1)
            tablePersonalInformation.addView(row2)
            tablePersonalInformation.addView(row3)
            tablePersonalInformation.addView(row4)
            tablePersonalInformation.addView(row5)
            tablePersonalInformation.addView(row6)
            tablePersonalInformation.addView(row7)
            tablePersonalInformation.addView(row8)

            val tablePreferences = findViewById<TableLayout>(R.id.table_preferences)
            val rowOne = TableRow(this)
            val bringPeopleTextView = createTextView("Bring People", 1f)
            val bringPeople = checkString(user.bring_people)
            val bringPeopleValueTextView = createTextView(bringPeople, 1f)
            rowOne.addView(bringPeopleTextView)
            rowOne.addView(bringPeopleValueTextView)

            val rowTwo = TableRow(this)
            val cleanTextView = createTextView("Clean the room", 1f)
            val clean = checkString(user.clean)
            val cleanValueTextView = createTextView(clean, 1f)
            rowTwo.addView(cleanTextView)
            rowTwo.addView(cleanValueTextView)

            val rowThree = TableRow(this)
            val sleepTextView = createTextView("Sleep time", 1f)
            val sleep = checkInt(user.sleep)
            val sleepValueTextView = createTextView(sleep, 1f)
            rowThree.addView(sleepTextView)
            rowThree.addView(sleepValueTextView)

            val rowFour = TableRow(this)
            val likesPetsTextView = createTextView("Likes pets", 1f)
            val likesPets = checkBoolean(user.likes_pets)
            val likesPetsValueTextView = createTextView(likesPets, 1f)
            rowFour.addView(likesPetsTextView)
            rowFour.addView(likesPetsValueTextView)

            val rowFive = TableRow(this)
            val smokeTextView = createTextView("Smoke", 1f)
            val smoke = checkBoolean(user.smoke)
            val smokeValueTextView = createTextView(smoke, 1f)
            rowFive.addView(smokeTextView)
            rowFive.addView(smokeValueTextView)

            val rowSix = TableRow(this)
            val vapeTextView = createTextView("Vape", 1f)
            val vape = checkBoolean(user.vape)
            val vapeValueTextView = createTextView(vape, 1f)
            rowSix.addView(vapeTextView)
            rowSix.addView(vapeValueTextView)

            tablePreferences.addView(rowOne)
            tablePreferences.addView(rowTwo)
            tablePreferences.addView(rowThree)
            tablePreferences.addView(rowFour)
            tablePreferences.addView(rowFive)
            tablePreferences.addView(rowSix)

        }
    }

    private fun checkBoolean(value: Boolean): String{
        return if (value){
            "Yes"
        } else {
            "No"
        }
    }

    private fun checkString(value: String): String{
        return if (value == ""){
            "Undefined"
        } else {
            value
        }
    }

    private fun checkInt(hour: Int): String{
        return if (hour == 0) {
            "12:00 AM"
        } else if (hour < 12) {
            "$hour:00 AM"
        } else if (hour == 12) {
            "12:00 PM"
        } else {
            "${hour - 12}:00 PM"
        }
    }

    private fun createTextView(text: String, weight: Float): TextView {
        val textView = TextView(this)
        val params = TableRow.LayoutParams(
            0, // Ancho 0
            TableRow.LayoutParams.WRAP_CONTENT,
            weight
        )

        textView.layoutParams = params
        textView.text = text
        textView.gravity = Gravity.CENTER
        textView.setPadding(8, 8, 8, 8)
        textView.setTextColor(resources.getColor(R.color.background_top))
        textView.textSize = 15f
        return textView
    }

    override fun showError(message: String) {


        runOnUiThread {

            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }
    }

    override fun showConnection()
    {

        runOnUiThread {
            dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet_lost)
            dialog.setCancelable(false)

            val retryButton = dialog.findViewById<Button>(R.id.btnRetry)
            retryButton.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    fun redirectToRegister(view: View) {

        val intent = Intent(this, ListApartmentActivity::class.java)
        startActivity(intent)
    }
}