package view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import com.example.android_kotlin.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.entity.ApartmentEntity
import model.entity.UserEntity
import model.utils.Connectivity
import utils.CachingImage
import utils.LRUCacheManager
import utils.LocalStorageUser
import utils.UserData

class DetailApartment: AppCompatActivity() {

    private lateinit var houseInfo : ApartmentEntity
    private lateinit var cacheManager : LRUCacheManager
    private lateinit var connectionChecker : Connectivity
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var user: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout
    private lateinit var nameView : TextView
    private lateinit var addressView : TextView
    private lateinit var priceView : TextView
    private lateinit var ratingView : TextView
    private lateinit var descriptionView : TextView
    private lateinit var areaView : TextView
    private lateinit var stratumView : TextView
    private lateinit var typeView : TextView
    private lateinit var roomView : TextView
    private lateinit var bathroomView : TextView
    private lateinit var floorView : TextView
    private lateinit var elevatorView : TextView
    private lateinit var furnishedView : TextView
    private lateinit var gymView : TextView
    private lateinit var internetView : TextView
    private lateinit var laundryView : TextView
    private lateinit var receptionView : TextView
    private lateinit var tvView : TextView
    private lateinit var supermarketView : TextView
    private lateinit var intent : Intent

    private var maxMemory = 0
    private var cacheSize = 0

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.detail_apartment)
        intent = getIntent()

        lifecycleScope.launch(Dispatchers.Main) {

            val keyUser = UserData.USER_LOGIN
            val user = loadUserFromSharedPreferences(keyUser)
            if (user == null) {
                showError("an error occurred, NO DATA")
            } else {
                menu(keyUser, user)
                cacheManager = LRUCacheManager()
                connectionChecker = Connectivity(applicationContext)
                maxMemory = (Runtime.getRuntime().maxMemory() / 1024).toInt()
                cacheSize = maxMemory / 15
                cacheManager.setCacheSize(cacheSize)

                lifecycleScope.launch(Dispatchers.IO) {
                    var key = intent.getStringExtra("detailed_apartment_name")
                    println("Llave actual :")
                    println(key)
                    if (cacheManager.size() > 0 && !connectionChecker.isNetworkAvailable() && key != null) {
                        getCacheData(key)
                    } else {
                        var startTime = System.currentTimeMillis()
                        var timeout = false
                        while (!connectionChecker.isNetworkAvailable()) {
                            var actualTime = System.currentTimeMillis()
                            var timeElapsed = (actualTime - startTime) / 1000
                            if ((timeElapsed > 5) && !timeout) {
                                runOnUiThread {
                                    printLoad()
                                }
                                timeout = true
                            }
                        }
                        getData(key!!)
                        println("Casa actual:")
                        println(houseInfo)
                        cacheManager.put(houseInfo.name, houseInfo)
                    }
                    loadViewData()
                    runOnUiThread { updateUI() }
                }
            }
        }
    }

    private fun menu(keyUser: String, users: UserEntity) {
        drawerLayout =  findViewById(R.id.drawer_layout_detail_apartment)
        menu =  findViewById(R.id.button_menu)
        home =  findViewById(R.id.home)
        user =  findViewById(R.id.users)
        apartmentOffers =  findViewById(R.id.apartments_offer)
        dashboard =  findViewById(R.id.dashboard)
        profile =  findViewById(R.id.profile)
        publish =  findViewById(R.id.publish)
        logout =  findViewById(R.id.logout)

        val foto = findViewById<ImageView>(R.id.nav_header_image)
        val nombre = findViewById<TextView>(R.id.nav_header_name_user)
        val email = findViewById<TextView>(R.id.nav_header_email)

        CachingImage.downloadImage(foto, users.image)
        nombre.text =  users.full_name
        email.text =  users.email
        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            recreate()
        }

        user.setOnClickListener {
            redirectActivity(this, ListUser::class.java)
        }
        apartmentOffers.setOnClickListener {
            redirectActivity(this, ListApartmentOffers::class.java)
        }
        profile.setOnClickListener { redirectActivity(this, Profile::class.java) }
        publish.setOnClickListener {
            redirectActivity(this, PublishApartmentLocation::class.java)
        }
        dashboard.setOnClickListener {
            redirectActivity(this, Dashboard::class.java)
        }
        logout.setOnClickListener {
            val d = LocalStorageUser.deleteUser(this, keyUser)
            Log.d("Valores", "Logout exitoso")
            redirectActivity(this, SignIn::class.java)
        }

    }

    private suspend fun loadUserFromSharedPreferences(keyUser: String): UserEntity? {
        return withContext(Dispatchers.Default) {
            return@withContext LocalStorageUser.getUserEntityFromSharedPreferences(this@DetailApartment, keyUser)
        }
    }

    private fun openDrawer(drawerLayout: DrawerLayout)
    {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout)
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity> )
    {
        val next: Intent =  Intent(activity, secondActivity)
        next.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause()
    {
        super.onPause()
        closeDrawer(drawerLayout)
    }

    private fun showError(message: String) {
        runOnUiThread {

            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }
    }

    private fun printLoad(){
        Toast.makeText(this, "The operation is taking more than expected. Check your internet connection", Toast.LENGTH_SHORT).show()
    }

    private fun updateUI() {
        nameView.text = houseInfo.name
        addressView.text = houseInfo.address
        priceView.text = houseInfo.rentPrice.toString() + "$"
        ratingView.text = houseInfo.rating.toString()
        descriptionView.text = houseInfo.description
        areaView.text = houseInfo.area.toString() + " m2"
        stratumView.text = houseInfo.stratum.toString()
        typeView.text = houseInfo.housingType
        roomView.text = houseInfo.roomsNumber.toString()
        bathroomView.text = houseInfo.bathroomsNumber.toString()
        floorView.text = houseInfo.apartmentFloor.toString()
        elevatorView.text = translateBoolean(houseInfo.elevator)
        furnishedView.text = translateBoolean(houseInfo.furnished)
        gymView.text = translateBoolean(houseInfo.gymnasium)
        internetView.text = translateBoolean(houseInfo.internet)
        laundryView.text = translateBoolean(houseInfo.laundryArea)
        receptionView.text = translateBoolean(houseInfo.reception)
        tvView.text = translateBoolean(houseInfo.tv)
        supermarketView.text = translateBoolean(houseInfo.supermarkets)
    }

    private fun loadViewData(){
        nameView = findViewById(R.id.housenameDetail)
        addressView = findViewById(R.id.houseaddressDetail)
        priceView = findViewById(R.id.housepriceDetail)
        ratingView = findViewById(R.id.houseratingDetail)
        descriptionView = findViewById(R.id.housedescriptionDetail)
        areaView = findViewById(R.id.houseareaDetail)
        stratumView = findViewById(R.id.housestratumDetail)
        typeView = findViewById(R.id.housetypeDetail)
        roomView = findViewById(R.id.houseroomsDetail)
        bathroomView = findViewById(R.id.housebathroomsDetail)
        floorView = findViewById(R.id.housefloorDetail)
        elevatorView = findViewById(R.id.houseelevatorDetail)
        furnishedView = findViewById(R.id.housefurnishedDetail)
        gymView = findViewById(R.id.housegymDetail)
        internetView = findViewById(R.id.houseinternetDetail)
        laundryView = findViewById(R.id.houselaundryDetail)
        receptionView = findViewById(R.id.housereceptionDetail)
        tvView = findViewById(R.id.housetvDetail)
        supermarketView = findViewById(R.id.housesupermarketDetail)
    }

    private fun translateBoolean(boolean: Boolean): String {
        var value = ""
        if (boolean) {
            value = "Yes"
        }
        else if (!boolean){
            value = "No"
        }
        return value
    }

    private fun getData(key: String) {
        houseInfo = intent.getSerializableExtra("detailed_apartment")!! as ApartmentEntity
    }

    private fun getCacheData(key : String) {
        houseInfo = cacheManager.get(key)
    }
}