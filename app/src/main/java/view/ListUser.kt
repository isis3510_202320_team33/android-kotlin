package view

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android_kotlin.R
import com.google.firebase.firestore.DocumentSnapshot
import contract.ListUserContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.entity.UserCardEntity
import model.entity.UserEntity
import model.interactor.ListUserInteractor
import model.interactor.UserInteractor
import presenter.ListApartmentAdapter
import presenter.ListUserPresenter
import presenter.UserPresenter
import utils.CachingImage
import utils.LocalStorageUser
import utils.UserData
import view.adapter.ListUserAdapter

class ListUser : AppCompatActivity(), ListUserContract.view {

    private lateinit var listUserPresenter: ListUserPresenter
    private lateinit var adapter: ListUserAdapter
    private lateinit var dialog: Dialog

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var user: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout

    private var lastVisibleDocument: DocumentSnapshot? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_user)

        lifecycleScope.launch(Dispatchers.Main) {

            val keyUser = UserData.USER_LOGIN
            val user = loadUserFromSharedPreferences(keyUser)
            if (user == null) {
                showError("an error occurred, NO DATA")
            } else {
                menu(keyUser, user)
                searchUsers()
            }
        }
    }


    private suspend fun loadUserFromSharedPreferences(keyUser: String): UserEntity? {
        return withContext(Dispatchers.Default) {
            return@withContext LocalStorageUser.getUserEntityFromSharedPreferences(this@ListUser, keyUser)
        }
    }

    private fun menu(keyUser: String, users: UserEntity)
    {
        drawerLayout =  findViewById(R.id.drawer_layout_list_user)
        menu =  findViewById(R.id.button_menu)
        home =  findViewById(R.id.home)
        user =  findViewById(R.id.users)
        apartmentOffers =  findViewById(R.id.apartments_offer)
        dashboard =  findViewById(R.id.dashboard)
        profile =  findViewById(R.id.profile)
        publish =  findViewById(R.id.publish)
        logout =  findViewById(R.id.logout)

        val foto = findViewById<ImageView>(R.id.nav_header_image)
        val nombre = findViewById<TextView>(R.id.nav_header_name_user)
        val email = findViewById<TextView>(R.id.nav_header_email)

        CachingImage.downloadImage(foto, users.image)
        nombre.text =  users.full_name
        email.text =  users.email

        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            redirectActivity(this, ListApartmentActivity::class.java)
        }
        user.setOnClickListener {
            recreate()
        }
        apartmentOffers.setOnClickListener {
            redirectActivity(this, ListApartmentOffers::class.java)
        }
        profile.setOnClickListener { redirectActivity(this, Profile::class.java) }
        publish.setOnClickListener {
            redirectActivity(this, PublishApartmentLocation::class.java)
        }
        dashboard.setOnClickListener {
            redirectActivity(this, Dashboard::class.java)
        }
        logout.setOnClickListener {
            val d = LocalStorageUser.deleteUser(this, keyUser)
            Log.d("Valores", "Logout exitoso")
            redirectActivity(this, SignIn::class.java)
        }
    }


    private fun searchUsers()
    {
        lifecycleScope.launch(Dispatchers.IO){
            val interactor = ListUserInteractor()
            listUserPresenter = ListUserPresenter(this@ListUser, interactor)
            listUserPresenter.getAllUsers(lastVisibleDocument)
        }
    }


    override fun showError(message: String) {
        runOnUiThread {

            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }
    }

    override fun showConnection()
    {
        runOnUiThread {

            dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet_lost)
            dialog.setCancelable(false)

            val retryButton = dialog.findViewById<Button>(R.id.btnRetry)
            retryButton.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()

        }
    }

    override fun showUserList(userList: ArrayList<UserCardEntity>, lastDocument: DocumentSnapshot?) {
        runOnUiThread {
            lastVisibleDocument = lastDocument
            Log.d("Valores", "Hi $userList")
            val recyclerView = findViewById<RecyclerView>(R.id.users_list)

            if (!::adapter.isInitialized) {
                adapter = ListUserAdapter(userList, object : ListUserAdapter.LoadMoreListener {
                    override fun onLoadMore() {
                        searchUsers()
                    }
                })
                recyclerView.layoutManager = LinearLayoutManager(this)
                recyclerView.adapter = adapter
            } else {
                adapter.addAll(userList)
            }
        }
    }

    private fun openDrawer(drawerLayout: DrawerLayout)
    {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout)
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity> )
    {
        val next: Intent =  Intent(activity, secondActivity)
        next.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause()
    {
        super.onPause()
        closeDrawer(drawerLayout)
    }

    override fun onBackPressed() {
        showError("You can't go back!!")
    }

    fun redirectToRegister(view: View) {

        val intent = Intent(this, ListApartmentActivity::class.java)
        startActivity(intent)
    }

}