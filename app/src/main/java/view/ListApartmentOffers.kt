package view

import CacheManager
import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PorterDuff
import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.SeekBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.android_kotlin.R
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.HouseDTO.HouseDTO
import model.entity.UserEntity
import model.repository.IApi.ApiApartmentOffers
import model.repository.IApi.ApiCounter
import model.utils.Connectivity
import presenter.ListApartmentOffersAdapter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import utils.CachingImage
import utils.LocalStorageUser
import utils.UserData

class ListApartmentOffers : AppCompatActivity() {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var connectionChecker : Connectivity
    private var distanceValue: Int? = 0

    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var user: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var houseRecyclerView : RecyclerView
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout


    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.list_apartment_offers)

        lifecycleScope.launch(Dispatchers.Main) {
            val keyUser  = UserData.USER_LOGIN

            val user = loadUserFromSharedPreferences(keyUser)
            if (user == null)
            {
                showError("an error occurred, NO DATA")
            }
            else{
                menu(keyUser, user)

                houseRecyclerView = findViewById(R.id.listHouses)
                connectionChecker = Connectivity(applicationContext)
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(this@ListApartmentOffers)

                val seek = findViewById<SeekBar>(R.id.seekBar)
                seek?.setOnSeekBarChangeListener(object :
                    SeekBar.OnSeekBarChangeListener
                { override fun onProgressChanged(seek: SeekBar, progress: Int, fromUser: Boolean){
                    val maxDistance = findViewById<TextView>(R.id.maxRange)
                    maxDistance.text = "$progress km"
                }

                    override fun onStartTrackingTouch(p0: SeekBar?) {
                        Log.d("resultado", "se movio")
                    }

                    override fun onStopTrackingTouch(p0: SeekBar?) {
                        Log.d("resultado", "se movio")
                    }
                })

                val cacheManager = CacheManager.getInstance()
                val localizeButton: Button = findViewById(R.id.localize)

                fun isNetworkAvailable(): Boolean {
                    return connectionChecker.isNetworkAvailable()
                }

                fun extractIntFromString(input: String): Int? {
                    val pattern = "\\d+".toRegex()
                    val matchResult = pattern.find(input)
                    return matchResult?.value?.toIntOrNull()
                }

                val sharedPreferences = getSharedPreferences("OffersShown", MODE_PRIVATE)

                fun showSatisfactionPopup(offerCount: Int) {
                    sharedPreferences.edit().putInt("offer_count", 0).apply()
                    val builder = AlertDialog.Builder(this@ListApartmentOffers)
                    builder.setTitle("Encuesta satisfaccion")
                    builder.setMessage("Ya has visto $offerCount ofertas, ¿no encuentras lo que buscas? ¿Nos ayudarias contestando una encuesta?")

                    builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                        Toast.makeText(applicationContext,
                            android.R.string.yes, Toast.LENGTH_SHORT).show()
                    }

                    builder.setNegativeButton(android.R.string.no) { dialog, which ->
                        Toast.makeText(applicationContext,
                            android.R.string.no, Toast.LENGTH_SHORT).show()
                    }

                    builder.setNeutralButton("Maybe") { dialog, which ->
                        Toast.makeText(applicationContext,
                            "Luego", Toast.LENGTH_SHORT).show()
                    }
                    builder.show()
                }

                var offerClickCounts = mutableMapOf<String, Int>()
                fun updateUI(houses: List<HouseDTO>) {
                    val adapter = ListApartmentOffersAdapter(houses){clickedId ->
                        val retrofit = Retrofit.Builder()
                            .baseUrl("http://34.123.214.132:8000/senehouse/")
                            .addConverterFactory(GsonConverterFactory.create())
                            .build()
                        val apiService = retrofit.create(ApiCounter::class.java)
                        GlobalScope.launch(Dispatchers.Main) {
                            try {
                                offerClickCounts[clickedId] = (offerClickCounts[clickedId] ?: 0) + 1
                                val response = apiService.updateCounter(clickedId)
                                if (response != null) {
                                    Log.d("resultadoresponse", clickedId)
                                    Log.d("resultadoid", response.toString())
                                } else {
                                    Log.e("ApiCounter", "Failed to update counter: ")
                                }
                                /*apiService.updateCounter(clickedId)*/
                                Log.d("resultadoid", offerClickCounts.toString())
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }
                    houseRecyclerView.adapter = adapter
                    houseRecyclerView.layoutManager = LinearLayoutManager(this@ListApartmentOffers)
                    var offerCount = sharedPreferences.getInt("offer_count", 0)
                    if (offerCount < 10) {
                        sharedPreferences.edit().putInt("offer_count", offerCount + adapter.itemCount).apply()
                    }
                    else{
                        showSatisfactionPopup(offerCount)
                    }

                }

                val retrofit = Retrofit.Builder()
                    .baseUrl("http://34.123.214.132:8000/senehouse/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

                val apiService = retrofit.create(ApiApartmentOffers::class.java)
                fun onButtonClick(maxDistance: Int?, latitude: Double, longitude: Double) {
                    CoroutineScope(Dispatchers.IO).launch {
                        try {
                            val houses = apiService.getNearestOffers(maxDistance, longitude, latitude)
                            cacheManager.saveToCache("houses", houses)
                            Log.d("resultado", houses.toString())

                            withContext(Dispatchers.Main) {
                                updateUI(houses)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }

                fun requestLocation() {
                    fusedLocationClient.lastLocation
                        .addOnSuccessListener { location: Location? ->
                            if (location != null) {
                                val latitude = location.latitude
                                val longitude = location.longitude
                                onButtonClick(distanceValue,latitude,longitude)
                            }
                        }
                }

                val locationPermissionCode = 123
                fun checkLocationPermission() {
                    if (ActivityCompat.checkSelfPermission(
                            this@ListApartmentOffers,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        ActivityCompat.requestPermissions(
                            this@ListApartmentOffers,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            locationPermissionCode
                        )
                    } else {
                        requestLocation()
                    }
                }

                localizeButton.setOnClickListener {
                    if (isNetworkAvailable()) {
                        val localizeText: TextView = findViewById(R.id.maxRange)
                        distanceValue = extractIntFromString(localizeText.text.toString())
                        checkLocationPermission()
                    } else {
                        val cachedData = cacheManager.getFromCache("houses")
                        if (cachedData != null){
                            updateUI(cachedData)
                            showError("No internet connection. Showing cached data.")
                        }
                        else {
                            showError("No internet connection and no cached data.")
                        }
                    }
                }
            }

        }

    }

    private fun menu(keyUser: String, users: UserEntity)
    {
        drawerLayout =  findViewById(R.id.drawer_layout_apartment_offer)
        menu =  findViewById(R.id.button_menu)
        home =  findViewById(R.id.home)
        user =  findViewById(R.id.users)
        apartmentOffers =  findViewById(R.id.apartments_offer)
        dashboard =  findViewById(R.id.dashboard)
        profile =  findViewById(R.id.profile)
        publish =  findViewById(R.id.publish)
        logout =  findViewById(R.id.logout)

        val foto = findViewById<ImageView>(R.id.nav_header_image)
        val nombre = findViewById<TextView>(R.id.nav_header_name_user)
        val email = findViewById<TextView>(R.id.nav_header_email)

        CachingImage.downloadImage(foto, users.image)
        nombre.text =  users.full_name
        email.text =  users.email

        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            redirectActivity(this, ListApartmentActivity::class.java)
        }
        user.setOnClickListener {
            redirectActivity(this, ListUser::class.java)
        }
        apartmentOffers.setOnClickListener {
            recreate()
        }
        profile.setOnClickListener { redirectActivity(this, Profile::class.java) }
        publish.setOnClickListener {
            redirectActivity(this, PublishApartmentLocation::class.java)
        }
        dashboard.setOnClickListener {
            redirectActivity(this, Dashboard::class.java)
        }
        logout.setOnClickListener {
            val d = LocalStorageUser.deleteUser(this, keyUser)
            Log.d("Valores", "Logout exitoso")
            redirectActivity(this, SignIn::class.java)
        }

        val seek = findViewById<SeekBar>(R.id.seekBar)
        seek?.setOnSeekBarChangeListener(object :
            SeekBar.OnSeekBarChangeListener
            { override fun onProgressChanged(seek: SeekBar, progress: Int, fromUser: Boolean){
                val maxDistance = findViewById<TextView>(R.id.maxRange)
                maxDistance.text = "$progress km"
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
                Log.d("resultado", "se movio")
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
                Log.d("resultado", "se movio")
            }
        })

        val cacheManager = CacheManager.getInstance()
        val localizeButton: Button = findViewById(R.id.localize)

        fun isNetworkAvailable(): Boolean {
            return connectionChecker.isNetworkAvailable()
        }

        fun extractIntFromString(input: String): Int? {
            val pattern = "\\d+".toRegex()
            val matchResult = pattern.find(input)
            return matchResult?.value?.toIntOrNull()
        }

        val sharedPreferences = getSharedPreferences("OffersShown", MODE_PRIVATE)

        fun showSatisfactionPopup(offerCount: Int) {
            sharedPreferences.edit().putInt("offer_count", 0).apply()
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Encuesta satisfaccion")
            builder.setMessage("Ya has visto $offerCount ofertas, ¿no encuentras lo que buscas? ¿Nos ayudarias contestando una encuesta?")

            builder.setPositiveButton(android.R.string.yes) { dialog, which ->
                Toast.makeText(applicationContext,
                    android.R.string.yes, Toast.LENGTH_SHORT).show()
            }

            builder.setNegativeButton(android.R.string.no) { dialog, which ->
                Toast.makeText(applicationContext,
                    android.R.string.no, Toast.LENGTH_SHORT).show()
            }

            builder.setNeutralButton("Maybe") { dialog, which ->
                Toast.makeText(applicationContext,
                    "Luego", Toast.LENGTH_SHORT).show()
            }
            builder.show()
        }

        var identificacion = "xd"
        fun offerItemClicked(houses: List<HouseDTO>, id: String) {
            Toast.makeText(this, "Clicked: ${id}", Toast.LENGTH_SHORT).show()
        }

        fun updateUI(houses: List<HouseDTO>) {
            val adapter = ListApartmentOffersAdapter(houses){clickedId ->
                val retrofit = Retrofit.Builder()
                    .baseUrl("http://34.123.214.132:8000/senehouse/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                identificacion = clickedId
                offerItemClicked(houses, identificacion)
                val apiService = retrofit.create(ApiCounter::class.java)
                GlobalScope.launch(Dispatchers.Main) {
                    try {
                        apiService.updateCounter(clickedId)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            houseRecyclerView.adapter = adapter
            houseRecyclerView.layoutManager = LinearLayoutManager(this)
            var offerCount = sharedPreferences.getInt("offer_count", 0)
            if (offerCount < 10) {
                sharedPreferences.edit().putInt("offer_count", offerCount + adapter.itemCount).apply()
            }
            else{
                showSatisfactionPopup(offerCount)
            }

        }

        val retrofit = Retrofit.Builder()
            .baseUrl("http://34.123.214.132:8000/senehouse/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val apiService = retrofit.create(ApiApartmentOffers::class.java)
        fun onButtonClick(maxDistance: Int?, latitude: Double, longitude: Double) {
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val houses = apiService.getNearestOffers(maxDistance, longitude, latitude)
                    cacheManager.saveToCache("houses", houses)
                    Log.d("resultado", houses.toString())

                    withContext(Dispatchers.Main) {
                        updateUI(houses)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }

        fun requestLocation() {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location: Location? ->
                    if (location != null) {
                        val latitude = location.latitude
                        val longitude = location.longitude
                        onButtonClick(distanceValue,latitude,longitude)
                    }
                }
        }

        val locationPermissionCode = 123
        fun checkLocationPermission() {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                    locationPermissionCode
                )
            } else {
                requestLocation()
            }
        }

        localizeButton.setOnClickListener {
            if (isNetworkAvailable()) {
                val localizeText: TextView = findViewById(R.id.maxRange)
                distanceValue = extractIntFromString(localizeText.text.toString())
                checkLocationPermission()
            } else {
                val cachedData = cacheManager.getFromCache("houses")
                if (cachedData != null){
                    updateUI(cachedData)
                    Toast.makeText(this, "No internet connection. Showing cached data.", Toast.LENGTH_SHORT).show()
                }
                else {
                    Toast.makeText(this, "No internet connection and no cached data.", Toast.LENGTH_SHORT).show()
                }
            }
        }

    }
    private fun openDrawer(drawerLayout: DrawerLayout)
    {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout)
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }
    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity> )
    {
        val next: Intent =  Intent(activity, secondActivity)
        next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause()
    {
        super.onPause()
        closeDrawer(drawerLayout)
    }

    override fun onBackPressed()
    {
        Toast.makeText(this, "Sorry", Toast.LENGTH_SHORT).show()
    }

    private suspend fun loadUserFromSharedPreferences(keyUser: String): UserEntity? {
        return withContext(Dispatchers.Default) {
            return@withContext LocalStorageUser.getUserEntityFromSharedPreferences(this@ListApartmentOffers, keyUser)
        }
    }

    private fun showError(message: String) {
        runOnUiThread {

            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }
    }
}