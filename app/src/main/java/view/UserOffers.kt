package view

import CacheManager
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.ImageButton
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.android_kotlin.R
import model.utils.Connectivity
import utils.LocalStorageUser
import utils.UserData

class UserOffers : AppCompatActivity() {
    private lateinit var houseRecyclerView: RecyclerView
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout
    private lateinit var connectionChecker : Connectivity
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.user_offers)

        houseRecyclerView = findViewById(R.id.houseList)
        drawerLayout = findViewById(R.id.drawer_layout_apartment_offer)
        menu = findViewById(R.id.button_menu)
        home = findViewById(R.id.home)
        dashboard = findViewById(R.id.dashboard)
        profile = findViewById(R.id.profile)
        publish = findViewById(R.id.publish)
        logout = findViewById(R.id.logout)
        connectionChecker = Connectivity(applicationContext)

        val keyUser = UserData.USER_LOGIN
        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            redirectActivity(this, ListApartmentActivity::class.java)
        }
        apartmentOffers.setOnClickListener {
            recreate()
        }
        profile.setOnClickListener { redirectActivity(this, Profile::class.java) }
        publish.setOnClickListener {
            redirectActivity(this, PublishApartmentLocation::class.java)
        }
        dashboard.setOnClickListener {
            redirectActivity(this, Dashboard::class.java)
        }
        logout.setOnClickListener {
            val d = LocalStorageUser.deleteUser(this, keyUser)
            Log.d("Valores", "Logout exitoso")
            redirectActivity(this, SignIn::class.java)
        }

        val cacheManager = CacheManager.getInstance()
        val localizeButton: TextView = findViewById(R.id.numberclicks)

        fun isNetworkAvailable(): Boolean {
            return connectionChecker.isNetworkAvailable()
        }

        val sharedPreferences = getSharedPreferences("OffersShown", MODE_PRIVATE)


        /*fun updateUI(houses: List<HouseDTO>) {
            val adapter = ListApartmentOffersAdapter(houses) { clickedId ->
                val retrofit = Retrofit.Builder()
                    .baseUrl("http://34.123.214.132:8000/senehouse/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                identificacion = clickedId
                offerItemClicked(houses, identificacion)
                val apiService = retrofit.create(ApiCounter::class.java)
                GlobalScope.launch(Dispatchers.Main) {
                    try {
                        apiService.updateCounter(clickedId)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            houseRecyclerView.adapter = adapter
            houseRecyclerView.layoutManager = LinearLayoutManager(this)

        }

        val retrofit = Retrofit.Builder()
            .baseUrl("http://34.123.214.132:8000/senehouse/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val apiService = retrofit.create(ApiApartmentOffers::class.java)
        fun onButtonClick(maxDistance: Int?, latitude: Double, longitude: Double) {
            CoroutineScope(Dispatchers.IO).launch {
                try {
                    val houses = apiService.getNearestOffers(maxDistance, longitude, latitude)
                    cacheManager.saveToCache("houses", houses)
                    Log.d("resultado", houses.toString())

                    withContext(Dispatchers.Main) {
                        updateUI(houses)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }*/
    }

    private fun openDrawer(drawerLayout: DrawerLayout)
    {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout)
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }
    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity> )
    {
        val next: Intent =  Intent(activity, secondActivity)
        next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause()
    {
        super.onPause()
        closeDrawer(drawerLayout)
    }

    override fun onBackPressed()
    {
        Toast.makeText(this, "Sorry", Toast.LENGTH_SHORT).show()
    }

}