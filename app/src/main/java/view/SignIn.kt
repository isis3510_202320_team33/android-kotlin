package view

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputFilter
import android.text.InputType
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.splashscreen.SplashScreen
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import androidx.lifecycle.lifecycleScope
import com.example.android_kotlin.R
import contract.UserContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import model.FirebaseAuthSingleton
import model.FirebaseStorageSingleton
import model.entity.UserEntity
import model.interactor.UserInteractor
import presenter.UserPresenter
import utils.ContextApplication
import utils.LocalStorageUser
import utils.UserData
import android.os.Handler
import android.os.Looper
import model.repository.model.db.AppDatabase
import services.MyService


class SignIn : AppCompatActivity(), UserContract.viewUser {

    private lateinit var userPresenter: UserPresenter
    private lateinit var buttonSignIn: Button
    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var dialog: Dialog

    private var progressDialog: AlertDialog? = null
    private lateinit var splashScreen: SplashScreen
    override fun onCreate(savedInstanceState: Bundle?) {
        splashScreen = installSplashScreen()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        ContextApplication.appContext
        AppDatabase.getDatabase(this)

        FirebaseDataBaseSingleton.init(this)
        FirebaseStorageSingleton.getInstance()
        FirebaseAuthSingleton.getInstance()

        startService()

        Handler(Looper.getMainLooper()).postDelayed({
            splashScreen.setKeepOnScreenCondition { false }
        }, 2000)

        buttonSignIn = findViewById(R.id.button_login)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)

        validationInputs()
        buttonSignIn.setOnClickListener {
            showProgressDialog()
            val emailS: String = email.text.toString()
            val passwordS: String = password.text.toString()
            lifecycleScope.launch(Dispatchers.IO) {
                setUp(emailS, passwordS)
            }

        }

    }

    private fun startService()
    {
        val intent = Intent(this, MyService::class.java)
        startService(intent)

    }
    override fun onBackPressed() {
        showError("You can't go back!!")
    }

    fun redirectToRegister(view: View) {
        val intent = Intent(this, RegisterUser::class.java)
        startActivity(intent)
    }

    suspend fun setUp(emailS: String, passwordS: String) {

        val interactor = UserInteractor()
        userPresenter = UserPresenter(this, interactor)
        userPresenter.authUser(emailS, passwordS)

    }

    override fun authSuccess(user: UserEntity) {
        lifecycleScope.launch (Dispatchers.Default){
            val context = applicationContext
            val keyUser  = UserData.USER_LOGIN
            val answer = LocalStorageUser.saveUserEntityToSharedPreferences(context, user, keyUser)
            runOnUiThread {
                if (!answer) {
                    showError("An error occurred while signing in.")
                } else {
                    hideProgressDialog()
                    nextPage()
                }
            }
        }
    }


    private fun nextPage() {

        val next: Intent = Intent(this, ListApartmentActivity::class.java)
        startActivity(next)
    }

    override fun showError(message: String) {
        runOnUiThread {
            hideProgressDialog()
            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }
    }


    override fun showConnection()
    {
        runOnUiThread {

            hideProgressDialog()
            dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet_sign_in)
            dialog.setCancelable(false)

            val retryButton = dialog.findViewById<Button>(R.id.btnRetry)
            retryButton.setOnClickListener {
                dialog.dismiss()
            }

            dialog.show()

        }
    }

    private fun validationInputs()
    {
        email.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        val inputFilterEmail = InputFilter { source, _, _, _, _, _ ->
            source.filter { it.isLetterOrDigit() || it in setOf('.', '_', '@') }
        }
        val maxLengthEmail = 40
        val inputFilterLengthEmail = InputFilter.LengthFilter(maxLengthEmail)
        email.filters = arrayOf(inputFilterEmail, inputFilterLengthEmail)

        password.inputType =  InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        val maxLengthPassword = 25
        val passwordLengthMax = InputFilter.LengthFilter(maxLengthPassword)
        password.filters = arrayOf(passwordLengthMax)

    }

    private fun showProgressDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setView(layoutInflater.inflate(R.layout.progress_dialog, null))
        builder.setCancelable(false)
        progressDialog = builder.create()
        progressDialog?.show()
    }

    private fun hideProgressDialog() {

        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }


}

