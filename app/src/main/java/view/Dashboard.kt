package view


import android.content.Intent
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TableLayout
import android.widget.TableRow
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import com.example.android_kotlin.R
import contract.DashboardContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.entity.UserEntity
import model.interactor.DashboardInteractor
import presenter.DashboardPresenter
import utils.CachingImage
import utils.LocalStorageUser
import utils.UserData
import java.text.NumberFormat
import java.util.Currency

class Dashboard : AppCompatActivity(), DashboardContract.view{

    private lateinit var dashboardPresenter: DashboardPresenter
    private lateinit var updateButton: Button
    private lateinit var dialog: Dialog
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var user: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboard)


        lifecycleScope.launch(Dispatchers.Main) {

            val keyUser  = UserData.USER_LOGIN
            val user = loadUserFromSharedPreferences(keyUser)
            if (user == null)
            {
                showError("an error occurred, NO DATA")
            }
            else{
                menu(keyUser, user)
                lifecycleScope.launch(Dispatchers.IO) {
                    searchData()
                }
                updateButton = findViewById(R.id.button_update)
                updateButton.setOnClickListener {
                    lifecycleScope.launch(Dispatchers.IO) {
                        searchData()
                    }
                }
            }

        }
    }

    private suspend fun loadUserFromSharedPreferences(keyUser: String): UserEntity? {
        return withContext(Dispatchers.Default) {
            return@withContext LocalStorageUser.getUserEntityFromSharedPreferences(this@Dashboard, keyUser)
        }
    }

    private fun menu(keyUser: String, users: UserEntity)
    {
        drawerLayout =  findViewById(R.id.drawer_layout_dashboard)
        menu =  findViewById(R.id.button_menu)
        home =  findViewById(R.id.home)
        user =  findViewById(R.id.users)
        apartmentOffers =  findViewById(R.id.apartments_offer)
        dashboard =  findViewById(R.id.dashboard)
        profile =  findViewById(R.id.profile)
        publish =  findViewById(R.id.publish)
        logout =  findViewById(R.id.logout)

        val foto = findViewById<ImageView>(R.id.nav_header_image)
        val nombre = findViewById<TextView>(R.id.nav_header_name_user)
        val email = findViewById<TextView>(R.id.nav_header_email)

        CachingImage.downloadImage(foto, users.image)
        nombre.text =  users.full_name
        email.text =  users.email

        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            redirectActivity(this, ListApartmentActivity::class.java)
        }
        user.setOnClickListener {
            redirectActivity(this, ListUser::class.java)
        }
        apartmentOffers.setOnClickListener {
            redirectActivity(this, ListApartmentOffers::class.java)
        }
        profile.setOnClickListener { redirectActivity(this, Profile::class.java) }
        publish.setOnClickListener {
            redirectActivity(this, PublishApartmentLocation::class.java)
        }
        dashboard.setOnClickListener {
            recreate()
        }
        logout.setOnClickListener {
            LocalStorageUser.deleteUser(this, keyUser)
            redirectActivity(this, SignIn::class.java)
        }
    }

    private fun openDrawer(drawerLayout: DrawerLayout)
    {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout)
    {
        if (drawerLayout.isDrawerOpen(GravityCompat.START))
        {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }
    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity> )
    {
        val next =  Intent(activity, secondActivity)
        next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause()
    {
        super.onPause()
        closeDrawer(drawerLayout)
    }


    private suspend fun searchData()
    {
        val keyUser  = UserData.USER_LOGIN
        val user = loadUserFromSharedPreferences(keyUser)
        if (user == null)
        {
            showError("an error occurred")
        }
        else
        {
            val interactor = DashboardInteractor()
            dashboardPresenter =  DashboardPresenter(this,interactor)
            dashboardPresenter.getApartments(this, user!!.id)
        }

    }


    override fun insertValues(rate: HashMap<String, List<String>>, revenue: HashMap<String, List<String>>) {
        runOnUiThread {

            val tableRate = findViewById<TableLayout>(R.id.table_rate)
            val tableRevenue = findViewById<TableLayout>(R.id.tabla_revenue)

            if (tableRate.childCount > 1 && tableRevenue.childCount > 1) {

                for (i in tableRate.childCount - 1 downTo 1) {

                    tableRate.removeViewAt(i)
                }

                for (i in tableRevenue.childCount - 1 downTo 1) {

                    tableRevenue.removeViewAt(i)
                }
            }
            for ((apartmentName, values) in rate) {
                val row = TableRow(this)

                val houseTextView = createTextView(apartmentName, 1f)
                val roomsTotalTextView = createTextView(values[0], 1f)
                val occupancyTextView = createTextView(values[1], 1f)
                val percentTextView = createTextView(values[2], 1f)

                row.addView(houseTextView)
                row.addView(roomsTotalTextView)
                row.addView(occupancyTextView)
                row.addView(percentTextView)

                tableRate.addView(row)
            }

            for ((apartmentName, values) in revenue) {
                val row = TableRow(this)

                val houseTextView = createTextView(apartmentName, 1f) // Peso 1
                val occupancyTextView = createTextView(values[0], 1f) // Peso 1
                val priceTextView = createCurrencyTextView(values[1].toDouble(), 1f) // Peso 1
                val revenueTextView = createCurrencyTextView(values[2].toDouble(), 1f) // Peso 1

                row.addView(houseTextView)
                row.addView(occupancyTextView)
                row.addView(priceTextView)
                row.addView(revenueTextView)

                tableRevenue.addView(row)
            }
        }
    }


    private fun createTextView(text: String, weight: Float): TextView {
        val textView = TextView(this)
        val params = TableRow.LayoutParams(
            0,
            TableRow.LayoutParams.WRAP_CONTENT,
            weight
        )

        textView.layoutParams = params
        textView.text = text
        textView.gravity = Gravity.CENTER
        textView.setPadding(8, 8, 8, 8)
        textView.setTextColor(resources.getColor(R.color.background_top))
        textView.textSize = 13f
        return textView
    }

    private fun createCurrencyTextView(value: Double, weight: Float): TextView {
        val textView = TextView(this)
        val params = TableRow.LayoutParams(
            0,
            TableRow.LayoutParams.WRAP_CONTENT,
            weight
        )

        val format = NumberFormat.getCurrencyInstance()
        format.currency = Currency.getInstance("COP")
        val formattedValue = format.format(value)

        textView.layoutParams = params
        textView.text = formattedValue
        textView.gravity = Gravity.CENTER
        textView.setPadding(8, 8, 8, 8)
        textView.setTextColor(resources.getColor(R.color.background_top))
        textView.textSize = 10f
        return textView
    }


    override fun showError(message: String) {
        runOnUiThread {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
        }

    }


    override fun showConnection()
    {
        runOnUiThread {

            dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet_lost)
            dialog.setCancelable(false)

            val retryButton = dialog.findViewById<Button>(R.id.btnRetry)
            retryButton.setOnClickListener {
                dialog.dismiss()
                lifecycleScope.launch {
                    searchData()

                }
            }

            dialog.show()

        }
    }

    fun redirectToRegister(view: View) {

        val intent = Intent(this, ListApartmentActivity::class.java)
        startActivity(intent)
    }

    override fun onBackPressed()
    {
        Toast.makeText(this, "Sorry", Toast.LENGTH_SHORT).show()
    }

}