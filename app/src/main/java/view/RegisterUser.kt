package view

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.os.Handler
import android.text.InputFilter
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import com.example.android_kotlin.R
import contract.IDialog
import contract.RegisterUserContract
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import presenter.RegisterUserPresenter
import model.interactor.RegisterUserInteractor
import model.utils.Connectivity
import utils.ContextApplication
import utils.LocalStorageUser
import utils.UserData

class RegisterUser : AppCompatActivity(), RegisterUserContract.viewRegisterUser {

    private lateinit var fullName: EditText
    private lateinit var email: EditText
    private lateinit var password: EditText
    private lateinit var age: EditText
    private lateinit var phone: EditText
    private lateinit var genre: Spinner
    private lateinit var city: Spinner
    private lateinit var locality: Spinner
    private lateinit var renterButton: ImageButton
    private lateinit var landlordButton: ImageButton

    private lateinit var registerButton: Button
    private lateinit var registerUserPresenter: RegisterUserPresenter
    private lateinit var dialog: Dialog
    private lateinit var connectivity: Connectivity
    private var progressDialog: AlertDialog? = null

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register_user)
        fullName = findViewById(R.id.full_name)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        age = findViewById(R.id.age)
        phone = findViewById(R.id.phone)
        genre = findViewById(R.id.genreCity)
        city = findViewById(R.id.spinnerCity)
        locality = findViewById(R.id.spinnerLocality)

        selectCityAndLocality()

        selectGenre()

        validationInputs()

        registerButton = findViewById(R.id.button_register)
        registerButton.setOnClickListener {
            runOnUiThread {
                transformAttributes()
            }

        }
        drawBorderBlue()
    }


    fun redirectToSignIn(view: View) {

        val intent = Intent(this, SignIn::class.java)
        startActivity(intent)
    }

    override fun onBackPressed() {
        showError("You can't go back!!")
    }

    override fun showError(message: String) {
        runOnUiThread {
            hideProgressDialog()
            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
        }
    }


    private fun drawBorderBlue() {
        renterButton = findViewById(R.id.renter)
        landlordButton = findViewById(R.id.landlord)

        val blueBorderColor = Color.parseColor("#2E5EAA")
        val blueBorder = GradientDrawable()
        blueBorder.setStroke(6, blueBorderColor)
        blueBorder.setColor(Color.WHITE)
        val cornerRadius = resources.getDimensionPixelSize(R.dimen.border_section)
        blueBorder.cornerRadius = cornerRadius.toFloat()


        val defaultBorder = GradientDrawable()
        defaultBorder.setStroke(6, Color.WHITE)
        defaultBorder.setColor(Color.WHITE)
        defaultBorder.cornerRadius = cornerRadius.toFloat()

        setButtonDeselected(renterButton, defaultBorder)
        setButtonDeselected(landlordButton, defaultBorder)

        renterButton.setOnClickListener {
            setButtonSelected(renterButton, blueBorder)
            setButtonDeselected(landlordButton, defaultBorder)
        }

        landlordButton.setOnClickListener {
            setButtonSelected(landlordButton, blueBorder)
            setButtonDeselected(renterButton, defaultBorder)
        }
    }

    private fun setButtonSelected(button: ImageButton, blueBorder: Drawable) {
        button.background = blueBorder
        button.isClickable = false
    }

    private fun setButtonDeselected(button: ImageButton, defaultBorder: Drawable) {
        button.background = defaultBorder
        button.isClickable = true
    }


    private fun transformAttributes() {

        val fullNameS = fullName.text.toString()
        val emailS = email.text.toString()
        val passwordS = password.text.toString()
        val ageS = age.text.toString()

        val phoneS = phone.text.toString()
        val genreS = genre.selectedItem.toString()
        val cityS = city.selectedItem.toString()
        val localityS = locality.selectedItem.toString()

        Log.d("Valores", "FullName: $fullNameS")
        Log.d("Valores", "Email: $emailS")
        Log.d("Valores", "Password: $passwordS")
        Log.d("Valores", "Age: $ageS")
        Log.d("Valores", "Phone: $phoneS")
        Log.d("Valores", "Gender: $genreS")
        Log.d("Valores", "City: $cityS")
        Log.d("Valores", "Locality: $localityS")
        Log.d("Valores", "Renter: ${renterButton.isClickable}")
        Log.d("Valores", "Landlord: ${landlordButton.isClickable}")

        val imageResource = R.drawable.profile_user
        val imageBitmap = BitmapFactory.decodeResource(resources, imageResource)

        /*
        lifecycleScope.launch(Dispatchers.IO) {
            val interactor = RegisterUserInteractor()
            registerUserPresenter = RegisterUserPresenter(this@RegisterUser, interactor)
            registerUserPresenter.registerUser(
                fullNameS, emailS, passwordS, ageS, phoneS, genreS, cityS,
                localityS, renterButton.isClickable, landlordButton.isClickable, imageBitmap
            )
        }*/
        sendData(fullNameS, emailS, passwordS, ageS, phoneS, genreS,
            cityS, localityS, imageBitmap
        )
    }

    private fun sendData(
        fullName: String, email: String, password: String, age: String,
        phone: String, genre: String, city: String, locality: String,
        imageBitmap: Bitmap
    ) {
            connectivity = Connectivity(ContextApplication.appContext)
            val connection = connectivity.isNetworkAvailable()

            if (connection) {

                val interactor = RegisterUserInteractor()
                registerUserPresenter = RegisterUserPresenter(this, interactor)
                registerUserPresenter.registerUser(
                    fullName, email, password, age, phone, genre, city,
                    locality, renterButton.isClickable, landlordButton.isClickable, imageBitmap
                )

            } else {
                showConnection()
            }
    }

    override fun confirmData(
        fullName: String, email: String, password: String, age: String,
        phone: String, genre: String, city: String, locality: String,
        imageBitmap: Bitmap
    ) {
        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Confirmación")
        alertDialogBuilder.setMessage("Aún puedes modificar la información")


        alertDialogBuilder.setPositiveButton("Aceptar") { dialog, which ->
            sendData(fullName, email, password, age, phone, genre,
                city, locality, imageBitmap)
        }

        alertDialogBuilder.setNegativeButton("Cancelar") { dialog, which -> }

        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()
    }

    override  fun showConfirmationDialog(listener: IDialog) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Confirm registration")
        builder.setMessage("Are you sure you want to register this data?")
        builder.setPositiveButton("Confirm") { dialog, which ->
            showProgressDialog()
            lifecycleScope.launch(Dispatchers.IO) {
                listener.onConfirm()
            }
        }

        builder.setNegativeButton("Cancel") {dialog, which ->
            listener.onCancel()
        }

        val dialog = builder.create()
        dialog.show()
    }



    override fun showConnection()
    {
        runOnUiThread {
            hideProgressDialog()
            dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_internet_register)
            dialog.setCancelable(false)

            val retryButton = dialog.findViewById<Button>(R.id.btnRetry)
            retryButton.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }
    }

    override fun registerUser()
    {
        hideProgressDialog()
        /*This code allows you to go to the next box for the requirement to publish a house.*/
        val next: Intent =  Intent(this, SignIn:: class.java)
        startActivity(next)

    }

    private fun validationInputs()
    {
        fullName.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS
        val inputFilterName = InputFilter { source, _, _, _, _, _ ->
            source.filter { it.isLetter() || it.isWhitespace() }
        }
        val maxLengthName = 40
        val inputFilterLengthName = InputFilter.LengthFilter(maxLengthName)
        fullName.filters = arrayOf(inputFilterName, inputFilterLengthName)

        email.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS
        val inputFilterEmail = InputFilter { source, _, _, _, _, _ ->
            source.filter { it.isLetterOrDigit() || it in setOf('.', '_', '@') }
        }
        val maxLengthEmail = 40
        val inputFilterLengthEmail = InputFilter.LengthFilter(maxLengthEmail)
        email.filters = arrayOf(inputFilterEmail, inputFilterLengthEmail)


        password.inputType =  InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        val maxLengthPassword = 25
        val passwordLengthMax = InputFilter.LengthFilter(maxLengthPassword)
        password.filters = arrayOf(passwordLengthMax)


        age.inputType = InputType.TYPE_CLASS_NUMBER
        val minLength = 2
        val ageInputFilter = InputFilter.LengthFilter(minLength)
        age.filters = arrayOf(ageInputFilter)

        phone.inputType = InputType.TYPE_CLASS_PHONE
        val exactLength = 10
        val phoneExactLengthFilter = InputFilter.LengthFilter(exactLength)
        phone.filters = arrayOf(phoneExactLengthFilter)
        phone.keyListener = DigitsKeyListener.getInstance("0123456789")

    }

    private fun selectCityAndLocality()
    {
        city.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                val selectedCity = parentView.getItemAtPosition(position).toString()
                val localitiesArrayId = when (selectedCity) {
                    "Select a city" -> R.array.localities_nothing
                    "Bogotá" -> R.array.localities_bogota
                    else -> R.array.empty_array
                }

                val localitiesArray = resources.getStringArray(localitiesArrayId)
                val localityAdapter = ArrayAdapter(this@RegisterUser, android.R.layout.simple_spinner_dropdown_item, localitiesArray)
                locality.adapter = localityAdapter
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
            }
        }

    }

    private fun selectGenre()
    {
        genre.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                val selectedCity = parentView.getItemAtPosition(position).toString()
            }
            override fun onNothingSelected(parentView: AdapterView<*>) {
            }
        }

    }

    override fun onResume() {
        super.onResume()
        val keyUser  = UserData.USER_REGISTER
        val user = LocalStorageUser.getUserEntityFromSharedPreferences(this, keyUser)
        if (user != null) {

            fullName.setText(user.full_name)
            email.setText(user.email)
            password.setText(user.password)
            age.setText((user.age).toString())
            phone.setText((user.phone).toString())
        }
    }

    private fun showProgressDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setView(layoutInflater.inflate(R.layout.progress_dialog, null))
        builder.setCancelable(false)
        progressDialog = builder.create()
        progressDialog?.show()
    }

    private fun hideProgressDialog() {

        progressDialog?.let {
            if (it.isShowing) {
                it.dismiss()
            }
        }
    }

}