package view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android_kotlin.R
import model.entity.UserCardEntity
import model.entity.UserEntity
import utils.CachingImage

class ListUserAdapter(private var userList : ArrayList<UserCardEntity>,
                      private val loadMoreListener: LoadMoreListener): RecyclerView.Adapter<ListUserAdapter.ViewHolder>() {

    interface LoadMoreListener {
        fun onLoadMore()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.card_layout, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val userCurrent = userList[i]
        if (userCurrent.image != "")
        {
            CachingImage.downloadImage(viewHolder.image, userCurrent.image )
            viewHolder.stars.text = userCurrent.stars.toString()
            viewHolder.name.text = userCurrent.full_name
        }
        else
        {
            if (userCurrent.imageDos != null)
            {
                viewHolder.image.setImageBitmap(userCurrent.imageDos)
                viewHolder.stars.text = userCurrent.stars.toString()
                viewHolder.name.text = userCurrent.full_name
            }
            else
            {
                viewHolder.image.setImageResource(R.drawable.warning)
                viewHolder.stars.text = userCurrent.stars.toString()
                viewHolder.name.text = userCurrent.full_name
            }

        }



        if (i == userList.size - 1) {
            loadMoreListener.onLoadMore()
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
    {
        val image = itemView.findViewById<ImageView>(R.id.image_preview)
        val stars = itemView.findViewById<TextView>(R.id.id_stars_value)
        val name = itemView.findViewById<TextView>(R.id.user_name_value)
    }

    fun addAll(newUsers: List<UserCardEntity>) {
        userList.addAll(newUsers)
        notifyDataSetChanged()
    }
}