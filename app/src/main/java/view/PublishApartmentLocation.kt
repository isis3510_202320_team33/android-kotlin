package view

import android.app.Activity
import android.content.Intent
import android.app.AlertDialog
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.android_kotlin.R
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import utils.ConstantsApartment
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.location.Address
import android.location.Geocoder
import android.os.Handler
import android.text.InputFilter
import android.text.InputType
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.widget.CompoundButtonCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import model.FirebaseAuthSingleton
import model.FirebaseStorageSingleton
import model.entity.UserEntity
import utils.CachingImage
import utils.ContextApplication
import utils.LocalStorageUser
import utils.UserData


class PublishApartmentLocation : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var googleMap: GoogleMap
    private lateinit var sharePreference: SharedPreferences
    private lateinit var name: EditText
    private lateinit var nameCity: Spinner
    private lateinit var nameNeighborhood: Spinner
    private lateinit var locationAddress: EditText
    private lateinit var apartmentCheck: CheckBox
    private lateinit var houseCheck: CheckBox
    private var latitudeS: Double = 0.0
    private var longitudeS: Double = 0.0
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var menu: ImageButton
    private lateinit var home: LinearLayout
    private lateinit var user: LinearLayout
    private lateinit var apartmentOffers: LinearLayout
    private lateinit var dashboard: LinearLayout
    private lateinit var profile: LinearLayout
    private lateinit var publish: LinearLayout
    private lateinit var logout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.publish_apartment_location)
        ContextApplication.appContext
        FirebaseDataBaseSingleton.init(this)
        FirebaseStorageSingleton.getInstance()
        FirebaseAuthSingleton.getInstance()

        lifecycleScope.launch(Dispatchers.Main){

            val keyUser  = UserData.USER_LOGIN
            val user = loadUserFromSharedPreferences(keyUser)
            if (user == null) {
                showError("an error occurred, No logueado")
            } else {
                menu(keyUser, user)
                name = findViewById(R.id.name_apartment)
                nameCity = findViewById(R.id.spinnerCityMunicipality)
                nameNeighborhood = findViewById(R.id.spinnerNeighborhood)
                locationAddress = findViewById(R.id.address)
                apartmentCheck = findViewById(R.id.check_apartment)
                houseCheck = findViewById(R.id.check_house)

                changeColorCheck()
                selectCityAndLocality()
                validationInputs()
                val sectionLocation = findViewById<TextView>(R.id.location)
                val continueButton = findViewById<Button>(R.id.button_uno)
                drawBorderBlue(sectionLocation)
                sharePreference = getSharedPreferences("DATA_HOUSE", Context.MODE_PRIVATE)
                val getName = sharePreference.getString("NAME", "")
                val getCity = sharePreference.getString("CITY", "")
                val getNeighborhood = sharePreference.getString("NEIGHBORHOOD", "")
                val getAddress = sharePreference.getString("ADDRESS", "")
                val getHousingType = sharePreference.getString("HOUSING_TYPE", "")
                val getLatitude = sharePreference.getString("LATITUDE", "")
                val getLongitude = sharePreference.getString("LONGITUDE", "")

                if (getName != "" && getCity != "" && getNeighborhood != "" && getAddress != "" &&
                    getHousingType != "" && getLatitude != "" && getLongitude != ""
                ) {
                    name.setText(getName)
                    val cityPosition = getPositionCity(nameCity, getCity!!)
                    val neighborhoodPosition = getPositionNeighborhood(getNeighborhood!!, getCity!!)
                    nameCity.setSelection(cityPosition)
                    nameNeighborhood.setSelection(neighborhoodPosition)
                    locationAddress.setText(getAddress)
                    latitudeS = convertStringToDouble(getLatitude!!)
                    longitudeS = convertStringToDouble(getLongitude!!)
                    if (getHousingType == ConstantsApartment.APARTMENT) {
                        apartmentCheck.isChecked = true
                        houseCheck.isChecked = false
                    } else {
                        apartmentCheck.isChecked = false
                        houseCheck.isChecked = true
                    }
                }
                continueButton.setOnClickListener {
                    validateAndProcessInput()
                }
            }
        }
    }

    private suspend fun loadUserFromSharedPreferences(keyUser: String): UserEntity? {
        return withContext(Dispatchers.Default) {
            return@withContext LocalStorageUser.getUserEntityFromSharedPreferences(this@PublishApartmentLocation, keyUser)
        }
    }

    private fun menu(keyUser: String, users: UserEntity){
        drawerLayout = findViewById(R.id.drawer_layout_publish)
        menu = findViewById(R.id.button_menu)
        home = findViewById(R.id.home)
        user =  findViewById(R.id.users)
        apartmentOffers = findViewById(R.id.apartments_offer)
        dashboard = findViewById(R.id.dashboard)
        profile = findViewById(R.id.profile)
        publish = findViewById(R.id.publish)
        logout = findViewById(R.id.logout)

        val foto = findViewById<ImageView>(R.id.nav_header_image)
        val nombre = findViewById<TextView>(R.id.nav_header_name_user)
        val email = findViewById<TextView>(R.id.nav_header_email)

        CachingImage.downloadImage(foto, users.image)
        nombre.text =  users.full_name
        email.text =  users.email

        menu.setOnClickListener { openDrawer(drawerLayout) }
        home.setOnClickListener {
            redirectActivity(this, ListApartmentActivity::class.java)
        }
        user.setOnClickListener {
            redirectActivity(this, ListUser::class.java)
        }
        apartmentOffers.setOnClickListener {
            redirectActivity(this, ListApartmentOffers::class.java)
        }
        profile.setOnClickListener { redirectActivity(this, Profile::class.java) }
        publish.setOnClickListener {
            recreate()
        }
        dashboard.setOnClickListener {
            redirectActivity(this, Dashboard::class.java)
        }
        logout.setOnClickListener {
            val d = LocalStorageUser.deleteUser(this, keyUser)
            redirectActivity(this, SignIn::class.java)
        }
    }

    private fun openDrawer(drawerLayout: DrawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeDrawer(drawerLayout: DrawerLayout) {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        }
    }

    private fun redirectActivity(activity: Activity, secondActivity: Class<out Activity>) {
        val next: Intent = Intent(activity, secondActivity)
        next.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        activity.startActivity(next)
        activity.finish()
    }

    override fun onPause() {
        super.onPause()
        closeDrawer(drawerLayout)
    }

    private fun drawBorderBlue(location: TextView) {
        val drawable = GradientDrawable()
        drawable.setColor(Color.parseColor("#2E5EAA"))
        val cornerRadius = resources.getDimensionPixelSize(R.dimen.border_section)
        drawable.cornerRadius = cornerRadius.toFloat()
        location.background = drawable
    }

    private fun dialogo() {

        val alertDialogBuilder = AlertDialog.Builder(this)
        alertDialogBuilder.setTitle("Confirmación dirección")
        alertDialogBuilder.setMessage("Está seguro de la dirección dada?")

        alertDialogBuilder.setPositiveButton("Aceptar") { dialog, which ->
            val next: Intent = Intent(this, PublishApartmentInformation::class.java)
            startActivity(next)
        }

        alertDialogBuilder.setNegativeButton("Cancelar") { dialog, which ->

        }
        val alertDialog = alertDialogBuilder.create()
        alertDialog.show()

    }

    private fun showError(message: String) {
            val delayMillis = 1000
            val handler = Handler()
            handler.postDelayed({
                val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
                val toastView = toast.view
                val customColor = ContextCompat.getColor(this, R.color.background_top)
                toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
                val text = toastView?.findViewById<TextView>(android.R.id.message)
                text?.setTextColor(Color.WHITE)
                text?.textSize = 18f
                toast.show()
            }, delayMillis.toLong())
    }

    private fun initMap() {
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(googleMap: GoogleMap) {
        this.googleMap = googleMap
        createMarker()
    }

    private fun createMarker() {

        val favoritePlace = LatLng(latitudeS, longitudeS)
        this.googleMap.addMarker(
            MarkerOptions().position(favoritePlace).title("Ubicación de la vivienda!")
        )
        this.googleMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(favoritePlace, 18f),
            4000,
            null
        )
    }

    private fun convertStringToDouble(number: String): Double {
        val numeroDouble = number.toDouble()
        return numeroDouble
    }

    private fun convertDoubleToString(number: Double): String {
        val numeroString = number.toString()
        return numeroString
    }

    private fun validateAndProcessInput()
    {
        val apartment = name.text.toString()
        val city = nameCity.selectedItem.toString()
        val neighborhood = nameNeighborhood.selectedItem.toString()
        val address = locationAddress.text.toString().trim()
        val isApartmentChecked = apartmentCheck.isChecked
        val isHouseChecked = houseCheck.isChecked

        if (apartment.isEmpty()) {
            showError("Please complete field name apartment.")
        }
        else if (city == "Select a city")
        {
            showError("Please complete field city.")
        }
        else if (neighborhood == "Select a locality")
        {
            showError("Please complete field neighborhood.")
        }
        else if (address.isEmpty())
        {
            showError("Please complete field address.")
        }
        else if ((!isApartmentChecked && !isHouseChecked) || (isApartmentChecked && isHouseChecked)) {
            showError("Por favor, seleccione al menos una opción (apartamento o casa).")
        } else {
            nextPage(apartment, city, neighborhood, address, isApartmentChecked)
        }
    }


    private fun nextPage(
        apartment: String, city: String,
        neighborhood: String, address: String,
        isApartmentChecked: Boolean
    ) {

        var housingType = ConstantsApartment.HOUSE
        if (isApartmentChecked) {
            housingType = ConstantsApartment.APARTMENT
        }

        getLatLngFromAddress(address, neighborhood, city)
        initMap()
        val editor = sharePreference.edit()
        editor.putString("NAME", apartment)
        editor.putString("CITY", city)
        editor.putString("NEIGHBORHOOD", neighborhood)
        editor.putString("ADDRESS", address)
        editor.putString("HOUSING_TYPE", housingType)
        editor.putString("LATITUDE", convertDoubleToString(latitudeS))
        editor.putString("LONGITUDE", convertDoubleToString(longitudeS))
        editor.apply()
        dialogo()

    }


    private fun getLatLngFromAddress(address: String, neighborhood: String, city: String) {

        val addressComplete = concatAddress(address, neighborhood, city)
        val coder = Geocoder(this)
        lateinit var address: List<Address>

        try {
            address = coder.getFromLocationName(addressComplete, 1) as List<Address>
            if (address == null) {

                showError("Fail to find Latitude  and Longitude")
            }
            val location = address[0]
            latitudeS = location.latitude
            longitudeS = location.longitude
        } catch (e: Exception) {
            showError("Fail to find Latitude  and Longitude")
        }

    }

    private fun concatAddress(address: String, neighborhood: String, city: String): String {
        val fullAddress = "$address, $neighborhood, $city".trim()
        return fullAddress
    }

    override fun onBackPressed() {
        showError("No puedes devolverte!")
    }
    private fun validationInputs()
    {
        name.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_CAP_WORDS
        val inputFilterName = InputFilter { source, _, _, _, _, _ ->
            source.filter { it.isLetter() || it.isWhitespace() }
        }
        val maxLengthName = 15
        val inputFilterLengthName = InputFilter.LengthFilter(maxLengthName)
        name.filters = arrayOf(inputFilterName, inputFilterLengthName)

        locationAddress.inputType = InputType.TYPE_TEXT_VARIATION_POSTAL_ADDRESS
        val minLength = 25
        val locationInputFilter = InputFilter.LengthFilter(minLength)
        val inputFilterAddress = InputFilter { source, _, _, _, _, _ ->
            source.filter { it.isLetterOrDigit() || it.isWhitespace() || it in setOf('\'', '-', '#') }
        }
        locationAddress.filters = arrayOf(locationInputFilter, inputFilterAddress)

    }

    private fun selectCityAndLocality()
    {
        nameCity.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parentView: AdapterView<*>, selectedItemView: View, position: Int, id: Long) {
                val selectedCity = parentView.getItemAtPosition(position).toString()
                val localitiesArrayId = when (selectedCity) {
                    "Select a city" -> R.array.localities_nothing
                    "Bogotá" -> R.array.localities_bogota
                    else -> R.array.empty_array
                }

                val localitiesArray = resources.getStringArray(localitiesArrayId)
                val localityAdapter = ArrayAdapter(this@PublishApartmentLocation, android.R.layout.simple_spinner_dropdown_item, localitiesArray)
                nameNeighborhood.adapter = localityAdapter
            }

            override fun onNothingSelected(parentView: AdapterView<*>) {
            }
        }

    }

    private fun getPositionCity(spinner: Spinner, itemName: String): Int {
        val adapter = spinner.adapter
        var position = 0
        for (i in 0 until adapter.count) {
            if (adapter.getItem(i).toString() == itemName) {
                position = i
            }
        }
        return position
    }

    private fun getPositionNeighborhood( nameNe: String, nameCity: String): Int {

        val localitiesArrayId = when (nameCity) {
            "Select a city" -> R.array.localities_nothing
            "Bogotá" -> R.array.localities_bogota
            else -> R.array.empty_array
        }

        val localitiesArray = resources.getStringArray(localitiesArrayId)
        val localityAdapter = ArrayAdapter(this@PublishApartmentLocation, android.R.layout.simple_spinner_item, localitiesArray)
        nameNeighborhood.adapter = localityAdapter

        var position = 0
        for (i in 0 until (localityAdapter).count) {
            if ( (localityAdapter).getItem(i).toString() == nameNe) {
                position = i
            }
        }
        return position
    }


    private fun changeColorCheck()
    {
        val selectedColor = ContextCompat.getColorStateList(this, R.color.background_top)
        CompoundButtonCompat.setButtonTintList(apartmentCheck, selectedColor)
        CompoundButtonCompat.setButtonTintList(houseCheck, selectedColor)
    }
}