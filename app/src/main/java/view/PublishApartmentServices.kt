package view

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.GradientDrawable
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.CompoundButtonCompat
import com.example.android_kotlin.R

class PublishApartmentServices : AppCompatActivity() {

    private lateinit var laundryArea: CheckBox
    private lateinit var internet: CheckBox
    private lateinit var tv: CheckBox
    private lateinit var furnished: CheckBox
    private lateinit var elevator: CheckBox
    private lateinit var smoke: CheckBox
    private lateinit var vape: CheckBox
    private lateinit var pets: CheckBox
    private lateinit var gym: CheckBox
    private lateinit var supermarket: CheckBox
    private lateinit var reception: CheckBox

    private lateinit var sharePreference: SharedPreferences
    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.publish_apartment_services)

        laundryArea = findViewById(R.id.check_laundry_area)
        internet = findViewById(R.id.check_internet)
        tv = findViewById(R.id.check_tv)
        furnished = findViewById(R.id.check_furnished)
        elevator = findViewById(R.id.check_elevator)
        smoke = findViewById(R.id.check_smoke)
        vape = findViewById(R.id.check_vape)
        pets = findViewById(R.id.check_pets)
        gym = findViewById(R.id.check_gym)
        supermarket = findViewById(R.id.check_supermarket)
        reception = findViewById(R.id.check_reception)

        changeColorCheck()
        val continueButton = findViewById<Button>(R.id.button_tres)
        val sectionServices =  findViewById<TextView>(R.id.services)
        drawBorderBlue(sectionServices)

        sharePreference = getSharedPreferences("DATA_HOUSE", Context.MODE_PRIVATE)

        val getLaundryArea = sharePreference.getBoolean("LAUNDRY_AREA", false)
        val getInternet = sharePreference.getBoolean("INTERNET", false)
        val getTv =  sharePreference.getBoolean("TV", false)
        val getFurnished =  sharePreference.getBoolean("FURNISHED", false)
        val getElevator = sharePreference.getBoolean("ELEVATOR", false)
        val getSmoke =  sharePreference.getBoolean("SMOKE", false)
        val getVape =  sharePreference.getBoolean("VAPE", false)
        val getPets = sharePreference.getBoolean("PETS", false)
        val getGym = sharePreference.getBoolean("GYM", false)
        val getSupermarket = sharePreference.getBoolean("SUPERMARKET", false)
        val getReception = sharePreference.getBoolean("RECEPTION", false)

        laundryArea.isChecked = getLaundryArea
        internet.isChecked = getInternet
        tv.isChecked = getTv
        furnished.isChecked = getFurnished
        elevator.isChecked = getElevator
        smoke.isChecked = getSmoke
        vape.isChecked = getVape
        pets.isChecked = getPets
        gym.isChecked =  getGym
        supermarket.isChecked = getSupermarket
        reception.isChecked =  getReception

        continueButton.setOnClickListener {

                validateAndProcessInput( laundryArea, internet, tv, furnished,
                    elevator, smoke, vape, pets, gym, supermarket, reception)

        }

        back_to()
    }

    private fun drawBorderBlue(location: TextView)
    {
        val drawable = GradientDrawable()
        drawable.setColor(Color.parseColor("#2E5EAA"))
        val cornerRadius = resources.getDimensionPixelSize(R.dimen.border_section)
        drawable.cornerRadius = cornerRadius.toFloat()
        location.background = drawable
    }

    fun validateAndProcessInput( laundryArea: CheckBox, internet: CheckBox, tv: CheckBox, furnished: CheckBox,
                                 elevator: CheckBox, smoke: CheckBox, vape: CheckBox, pets: CheckBox,
                                 gym: CheckBox, supermarket: CheckBox, reception: CheckBox)
    {

        val isLaundryArea = laundryArea.isChecked
        val isInternet = internet.isChecked
        val isTv = tv.isChecked
        val isFurnished = furnished.isChecked

        val isElevator = elevator.isChecked
        val isSmoke = smoke.isChecked
        val isVape = vape.isChecked
        val isPets = pets.isChecked
        val isGym =  gym.isChecked
        val isSupermarket = supermarket.isChecked
        val isReception =  reception.isChecked

        if (isLaundryArea || isInternet || isTv || isFurnished || isElevator || isSmoke
            || isVape || isPets || isGym || isSupermarket || isReception) {

            nextPage(isLaundryArea, isInternet, isTv, isFurnished, isElevator, isSmoke, isVape,
                isPets, isGym, isSupermarket, isReception)
        }
        else {
            showError("Please select at least one service.")
        }
    }

    fun showError(message: String)
    {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        val toastView = toast.view
        val customColor = ContextCompat.getColor(this, R.color.background_top)
        toastView?.background?.setColorFilter(customColor, PorterDuff.Mode.SRC_IN)
        val text = toastView?.findViewById<TextView>(android.R.id.message)
        text?.setTextColor(Color.WHITE)
        text?.textSize = 18f
        toast.show()

    }

    private fun nextPage(laundryArea: Boolean, internet: Boolean, tv: Boolean, furnished: Boolean,
                         elevator: Boolean, smoke: Boolean, vape: Boolean, pets: Boolean, gym: Boolean,
                         supermarket: Boolean, reception: Boolean)
    {


        val editor = sharePreference.edit()
        editor.putBoolean("LAUNDRY_AREA", laundryArea)
        editor.putBoolean("INTERNET", internet)
        editor.putBoolean("TV", tv)
        editor.putBoolean("FURNISHED", furnished)
        editor.putBoolean("ELEVATOR", elevator)
        editor.putBoolean("SMOKE", smoke)
        editor.putBoolean("VAPE", vape)
        editor.putBoolean("PETS", pets)
        editor.putBoolean("GYM", gym)
        editor.putBoolean("SUPERMARKET", supermarket)
        editor.putBoolean("RECEPTION", reception)
        editor.apply()
        val next: Intent =  Intent(this, PublishApartmentPhotos:: class.java)
        startActivity(next)
    }

    private fun back_to()
    {
        val continues_information: ImageButton =  findViewById(R.id.go_back_i)
        continues_information.setOnClickListener {

            val next: Intent =  Intent(this, PublishApartmentInformation:: class.java)
            startActivity(next)
        }
    }

    private fun changeColorCheck()
    {
        val selectedColor = ContextCompat.getColorStateList(this, R.color.background_top)
        CompoundButtonCompat.setButtonTintList(laundryArea, selectedColor)
        CompoundButtonCompat.setButtonTintList(internet, selectedColor)
        CompoundButtonCompat.setButtonTintList(tv, selectedColor)
        CompoundButtonCompat.setButtonTintList(furnished, selectedColor)
        CompoundButtonCompat.setButtonTintList(elevator, selectedColor)
        CompoundButtonCompat.setButtonTintList(smoke, selectedColor)
        CompoundButtonCompat.setButtonTintList(vape, selectedColor)
        CompoundButtonCompat.setButtonTintList(pets, selectedColor)
        CompoundButtonCompat.setButtonTintList(gym, selectedColor)
        CompoundButtonCompat.setButtonTintList(supermarket, selectedColor)
        CompoundButtonCompat.setButtonTintList(reception, selectedColor)
    }

}