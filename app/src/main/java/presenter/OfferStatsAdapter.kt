package presenter

import android.content.Context
import android.graphics.Bitmap

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.android_kotlin.R
import model.HouseDTO.HouseDTO
import java.io.FileOutputStream

class OfferStatsAdapter(private val houses: List<HouseDTO>) : RecyclerView.Adapter<OfferStatsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Define views inside the item layout and initialize them here
        val times_seen: TextView = itemView.findViewById(R.id.numberclicks)
        val casa: ImageView = itemView.findViewById(R.id.casa)

        fun bindImage(context: Context, imageUrl: String, fileName: String) {
            Glide.with(itemView)
                .asBitmap()
                .load(imageUrl)
                .placeholder(R.drawable.casa_verde)
                .error(R.drawable.casa_verde)
                .into(object : SimpleTarget<Bitmap>() {
                    override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                        // Save the bitmap to internal storage
                        saveBitmapToLocal(context, resource, fileName)

                        // Display the bitmap in the ImageView
                        casa.setImageBitmap(resource)
                    }
                })
        }

        private fun saveBitmapToLocal(context: Context, bitmap: Bitmap, fileName: String) {
            val outputStream: FileOutputStream
            try {
                outputStream = context.openFileOutput(fileName, Context.MODE_PRIVATE)
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)
                outputStream.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.offer_stats, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return houses.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val house = houses[position]
        holder.times_seen.text = house.rentPrice.toString()

        val fileName = "image_${house.idUser}.png"
        holder.bindImage(holder.casa.context, house.images.elementAt(0), fileName)
    }
}