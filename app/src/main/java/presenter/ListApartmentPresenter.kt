package presenter


import model.entity.ApartmentEntity
import model.repository.ApartmentRepository
import model.repository.remote.ApartmentRemote

class ListApartmentPresenter() {

    private var apartmentRepository = ApartmentRepository(ApartmentRemote())
    suspend fun getAllApartments(): ArrayList<ApartmentEntity>?{
        return apartmentRepository.getAllApartments()
    }

    fun setOriginalAvgs(houseArrayList: ArrayList<ApartmentEntity>): ArrayList<Int> {
        var avgsList = ArrayList<Int>()
        var roomAvg = 0
        var priceAvg = 0
        houseArrayList.forEach {
            roomAvg += it.roomsNumber
            priceAvg += it.rentPrice.toInt()
        }
        roomAvg /= houseArrayList.size
        priceAvg /= houseArrayList.size
        avgsList.add(roomAvg)
        avgsList.add(priceAvg)
        return avgsList
    }

}