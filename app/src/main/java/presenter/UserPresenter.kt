package presenter

import contract.UserContract
import model.interactor.UserInteractor

class UserPresenter(private val userView: UserContract.viewUser,
                    private val interactor: UserInteractor): UserContract.presenterUser {

    override suspend fun authUser(email: String, password: String){

        if (email.isEmpty() || password.isEmpty()) {
            userView.showError("Enter the credentials!!")
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches())
        {
            userView.showError("Invalid email.")
        }
        else if (password.length < 8)
        {
            userView.showError("The password must have at least 8 characters.")
        }
        else
        {
            val res =  interactor.sendDataUser(email, password)
            if (res == null)
            {
                userView.showError("Oops!! I think you are not registered")
            }
            else if (res.full_name == "No")
            {
                userView.showConnection()
            }
            else{
                userView.authSuccess(res)
            }

        }


    }


}