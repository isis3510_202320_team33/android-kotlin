package presenter

import android.content.Context
import android.util.Log
import contract.ProfileContract
import model.interactor.ProfileInteractor
import model.interactor.UserInteractor

class ProfilePresenter(private val view: ProfileContract.view,
                       private val interactor: ProfileInteractor
): ProfileContract.presenter{


    override suspend fun getProfile(email: String, password: String)
    {

        Log.d("Valores", "Capa presenter - profile")
        val respuesta =  interactor.getUser(email, password)
        if (respuesta == null)
        {
            view.showError("Oops!! An error occurred")
        }
        else if (respuesta.full_name == "No")
        {
            view.showConnection()
        }
        else{
            view.insertValues(respuesta)
        }

    }

}