package presenter

import android.content.Context
import android.graphics.Bitmap
import android.location.Address
import android.location.Geocoder
import android.util.Log
import contract.IDialog
import contract.RegisterUserContract
import kotlinx.coroutines.CoroutineScope
import model.entity.UserEntity
import model.interactor.RegisterUserInteractor
import utils.ContextApplication
import utils.UserType

class RegisterUserPresenter(private val viewRegisterUser: RegisterUserContract.viewRegisterUser,
                            private val interactor: RegisterUserInteractor): RegisterUserContract.presenterRegisterUser {

    companion object {
        val allowedPrefixes = listOf(
            "300", "301", "302", "303", "304", "324",
            "305", "310", "311", "312", "313", "314",
            "320", "321", "322", "323", "315", "316",
            "317", "318", "319", "319", "350", "351",
            "302", "323", "324", "324", "333", "333",
        )
    }

    override fun registerUser(fullName: String, email: String, password: String, age: String,
                                      phone:String, gender: String, city: String, locality: String,
                                      renter: Boolean, landlord: Boolean, image: Bitmap)
    {
        val phoneNumber = phone.trim()
        val coordenadas = getLatLngFromAddress(city, locality)

        if (fullName.isEmpty() ) {
            viewRegisterUser.showError("Please complete field full name.")
        }

        else if ( email.isEmpty())
        {
            viewRegisterUser.showError("Please complete field email.")
        }

        else if (password.isEmpty())
        {
            viewRegisterUser.showError("Please complete field password.")
        }
        else if (age.isEmpty() )
        {
            viewRegisterUser.showError("Please complete field age.")
        }

        else if (phone.isEmpty())
        {
            viewRegisterUser.showError("Please complete field phone.")
        }

        else if (gender == "Select a genre")
        {
            viewRegisterUser.showError("Please complete field gender.")
        }
        else if (city == "Select a city")
        {
            viewRegisterUser.showError("Please complete field city.")
        }
        else if (locality == "Select a locality")
        {
            viewRegisterUser.showError("Please complete field locality.")
        }
        else if ((renter && landlord))
        {
            viewRegisterUser.showError("Please select Renter or landlord")
        }

        else if ( coordenadas == null)
        {
            viewRegisterUser.showError("Try again later, we can not get coordinates!")
        }
        else if (!android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Log.d("Valores", "Email no válido")
            viewRegisterUser.showError("Invalid email.")
        }
        else if (password.length < 8) {
            Log.d("Valores", "La contraseña debe tener al menos 8 caracteres")
            viewRegisterUser.showError("The password must have at least 8 characters.")
        }

        else if ((age.toInt() < 15 ||  age.toInt() > 50)) {
            Log.d("Valores", "La edad mínima es 15 y máxima 50")
            viewRegisterUser.showError("The minimum age is 15, and the maximum is 50.")
        }

        else if (phone.length != 10) {
            viewRegisterUser.showError("Incomplete phone number.")
        }

        else if (!allowedPrefixes.any { phoneNumber.startsWith(it) }) {
            viewRegisterUser.showError("Verify that the number is correctly written.")
        }

        else {

        viewRegisterUser.showConfirmationDialog(object : IDialog {
            override suspend fun onConfirm() {
                var userType = UserType.LANDLORD
                if (!renter) {
                    userType = UserType.RENTER
                }
                val (latitude, longitude) = coordenadas
                Log.d("Valores", "Latitude: $latitude")
                Log.d("Valores", "Longitude: $longitude")
                val registerUserEntity = UserEntity(
                    full_name = fullName, email = email, password = password,
                    age = age.toInt(), phone = phone.toLong(), gender = gender, city = city, locality = locality,
                    latitude = latitude, longitude = longitude, rol = userType,
                )

                val respuesta = interactor.registerUser(registerUserEntity, image)
                if (respuesta == null) {
                    viewRegisterUser.showError("No se pudo guardar los datos")
                } else if (respuesta == "No connection") {
                    viewRegisterUser.showConnection()
                }
                else if (respuesta == "usuario existente") {
                    viewRegisterUser.showError("You cannot register with this email.")
                }
                else {
                    Log.d("Valores", "Validaciones exitosas.... LAYER PRESENTER $registerUserEntity")
                    viewRegisterUser.registerUser()
                }
            }

            override fun onCancel() {
            }
        })
        }
    }


    private fun getLatLngFromAddress(locality: String, city: String): Pair<Double, Double>? {
        val addressComplete = concatAddress(locality, city)
        val coder = Geocoder(ContextApplication.appContext)
        lateinit var address: List<Address>

        try {
            address = coder.getFromLocationName(addressComplete, 1) as List<Address>
            if (address.isEmpty()) {
                return null
            }
            val location = address[0]
            val latitudeS = location.latitude
            val longitudeS = location.longitude
            Log.d("Valores", "Latitude: $latitudeS")
            Log.d("Valores", "Longitude: $longitudeS")
            return Pair(latitudeS, longitudeS)
        } catch (e: Exception) {
            Log.e("Valores", "Fail to find Latitude and Longitude", e)
            return null
        }
    }

    private fun concatAddress(locality: String, city: String): String {
        val fullAddress = "$locality, $city".trim()
        return fullAddress
    }

}