package presenter

import android.content.Context
import contract.DashboardContract
import model.interactor.DashboardInteractor

class DashboardPresenter(private  val view: DashboardContract.view,
                         private val interactor: DashboardInteractor): DashboardContract.presenter{


    override suspend fun getApartments(context: Context, id: String) {
        try {
            val result = interactor.getApartments(context, id)
            if (result != null) {
                view.insertValues(result.occupancyRate, result.revenue)
            } else {
                view.showConnection()
            }

        } catch (e: Exception) {
            view.showError("Ocurrió un error. Intente nuevamente más tarde.")
        }
    }


}