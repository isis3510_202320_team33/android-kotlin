package presenter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.android_kotlin.R
import model.entity.ApartmentEntity


class ListApartmentAdapter(private var houseList : ArrayList<ApartmentEntity>) : RecyclerView.Adapter<ListApartmentAdapter.HouseViewHolder>(){

    private lateinit var mContext: Context
    var onItemClick : ((ApartmentEntity) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HouseViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(
            R.layout.list_apartment,
            parent, false)
        mContext = parent.context
        return HouseViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return houseList.size
    }

    override fun onBindViewHolder(holder: HouseViewHolder, position: Int) {

        val currentitem = houseList[position]
        holder.roomsNumber.text = currentitem.roomsNumber.toString()
        holder.rentPrice.text = currentitem.rentPrice.toString()
        holder.rating.text = currentitem.rating.toString()
        holder.address.text = currentitem.address
        holder.locality.text = currentitem.neighborhood
        holder.itemView.setOnClickListener {
            onItemClick?.invoke(currentitem)
        }
    }

    class HouseViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        val roomsNumber : TextView = itemView.findViewById(R.id.aptroomcount)
        val rentPrice : TextView = itemView.findViewById(R.id.aptrentprice)
        val rating : TextView = itemView.findViewById(R.id.aptratingavg)
        val address : TextView = itemView.findViewById(R.id.aptadress)
        val locality : TextView = itemView.findViewById(R.id.aptlocalityname)

    }

    fun setFilteredList(filteredHouseList: ArrayList<ApartmentEntity>){
        this.houseList = filteredHouseList
        notifyDataSetChanged()
    }

}