package presenter

import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import contract.ListUserContract
import contract.UserContract
import model.entity.UserCardEntity
import model.interactor.ListUserInteractor
import model.interactor.UserInteractor
import model.utils.Connectivity
import utils.ContextApplication
import utils.NumberPage


class ListUserPresenter (private val view: ListUserContract.view,
                         private val interactor: ListUserInteractor): ListUserContract.presenter {

    private lateinit var connectivity: Connectivity

    override suspend fun getAllUsers(lastDocument: DocumentSnapshot?) {

        // Eventual connectivity
        connectivity = Connectivity(ContextApplication.appContext)
        val connection = connectivity.isNetworkAvailable()
        Log.d("Valores", "answer connection: $connection")
        try {

            if (connection)
            {
                Log.d("Valores", "last document first: $lastDocument")
                val respuesta = interactor.getAllUsers(NumberPage.PAGE_SIZE, lastDocument )

                Log.d("Valores", "-----------start-----------------")
                Log.d("Valores", "last document first: $respuesta")
                Log.d("Valores", "-----------final-----------------")

                val userList = respuesta.first
                val resConnection = respuesta.third
                val newLastVisibleDocument = respuesta.second
                if (userList == null && newLastVisibleDocument == null && resConnection == "") {
                    view.showError("An error occurred while loading the users.")
                } else {

                    if (userList != null) {

                        if (userList.isEmpty() && newLastVisibleDocument == null && resConnection == "")
                        {
                            view.showError("There are not more data")
                        }
                        else {
                            view.showUserList(userList, newLastVisibleDocument)
                        }
                    }

                }
            }
            else
            {

                Log.d("Valores", "No connection:")
                if (interactor.getAllUsersCache().isNotEmpty())
                {
                    Log.d("Valores", "Datos en cache:")
                    val lis = interactor.getAllUsersCache()
                    Log.d("Valores", "cantidad usuarios caching:${lis.size}")
                    view.showUserList(lis, null)
                    view.showError("Remember, you don't have internet")
                }
                else{
                    Log.d("Valores", "No hay atos en cache ni internet")
                    view.showConnection()
                }

            }

        } catch (e: Exception) {
            Log.e("Valores", "Error en el interactor", e)

        }

    }
}