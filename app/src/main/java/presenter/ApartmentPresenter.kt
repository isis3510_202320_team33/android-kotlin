package presenter

import android.graphics.Bitmap
import contract.ApartmentContract
import model.entity.ApartmentEntity
import model.interactor.ApartmentInteractor

class ApartmentPresenter (private val apartmentPhotoView: ApartmentContract.viewPhoto,
                        private val interactor: ApartmentInteractor): ApartmentContract.presenter{

        override suspend fun  paramsValidation(apartment: String, city: String, neighborhood: String, address: String, housingType: String,
                                    latitude: String, longitude: String, rentPrice: String, stratum: String, area: String,
                                    apartmentFloor: String, roomsNumber: String, roomArea: String, bathroomsNumber: String,
                                       laundryArea: Boolean, internet: Boolean, tv: Boolean, furnished: Boolean,
                                    elevator: Boolean, smoke: Boolean, vape: Boolean, pets: Boolean, gym: Boolean,
                                               supermarket: Boolean, reception: Boolean,
                                       description: String, photos: List<Bitmap>, user: String
        )
        {

            val apartmentEntity = ApartmentEntity(apartment, city, neighborhood, address, housingType,
                latitude = latitude.toDouble(), longitude = longitude.toDouble(),
                rentPrice =  convertPricetoLong(rentPrice), stratum = stratum.toInt(),
                area = area.toDouble(), apartmentFloor = apartmentFloor.toInt(),
                roomsNumber = roomsNumber.toInt(), availableRoom = roomsNumber.toInt(),
                roomArea = roomArea.toDouble(), bathroomsNumber = bathroomsNumber.toInt(), laundryArea,
                internet, tv, furnished, elevator, smoke, vape, pets,
                gymnasium = gym,
                supermarkets =  supermarket,
                reception = reception,
                rating = 0, description)

            val answer =  interactor.insertApartment(apartmentEntity, photos, user )
            if (answer == null)
            {
                apartmentPhotoView.showError("An error occurred, please try again")
            }
            else if (answer == "No connection")
            {
                apartmentPhotoView.showConnection()
            }
            else
            {
                apartmentPhotoView.showError("The apartment has been successfully created.")
                apartmentPhotoView.nextPage()
            }

        }
    private fun convertPricetoLong(price: String): Long {
        val cleanPrice = price.replace("[^\\d]".toRegex(), "")
        return cleanPrice.toLong()
    }

}