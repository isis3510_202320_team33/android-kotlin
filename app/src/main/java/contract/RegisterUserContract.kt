package contract

import android.graphics.Bitmap

interface RegisterUserContract {

    interface viewRegisterUser
    {

         fun showConfirmationDialog(listener: IDialog)

        fun confirmData( fullName: String, email: String, password: String, age: String,
                         phone: String, genre: String, city: String, locality: String,
                         imageBitmap: Bitmap)
        fun showConnection()
        fun showError(message: String)
        fun registerUser()

    }

    interface presenterRegisterUser
    {

         fun registerUser(fullName: String, email: String, password: String, age: String,
                         phone:String, gender: String, city: String, locality: String,
                                 renter: Boolean, landlord: Boolean, image: Bitmap)
    }


}