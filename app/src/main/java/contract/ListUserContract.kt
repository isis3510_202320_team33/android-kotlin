package contract

import com.google.firebase.firestore.DocumentSnapshot
import model.entity.UserCardEntity
import model.entity.UserEntity

interface ListUserContract {
    interface view{
        fun showConnection()
        fun showError(message: String)

        fun showUserList(userList: ArrayList<UserCardEntity>, lastDocument: DocumentSnapshot?)
        {

        }
    }

    interface presenter {

        suspend fun getAllUsers(lastDocument: DocumentSnapshot?)
    }

}