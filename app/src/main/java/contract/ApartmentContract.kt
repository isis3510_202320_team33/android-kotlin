package contract
import android.graphics.Bitmap

interface ApartmentContract {

    interface viewPhoto{
        fun showConnection()
        fun showError(message: String)
        fun back_to()
        fun nextPage()
    }

    interface presenter{

        suspend fun  paramsValidation(
            apartment: String, city: String, neighborhood: String, address: String,
            housingType: String, latitude: String, longitude: String, rentPrice: String,
            stratum: String, area: String, apartmentFloor: String, roomsNumber: String,
            roomArea: String, bathroomsNumber: String, laundryArea: Boolean,
            internet: Boolean, tv: Boolean, furnished: Boolean, elevator: Boolean, smoke: Boolean,
            vape: Boolean, pets: Boolean, gym: Boolean, supermarket: Boolean, reception: Boolean,
            description: String, photos: List<Bitmap>, user: String)
    }

}