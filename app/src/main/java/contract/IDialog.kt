package contract

interface IDialog {

    suspend fun onConfirm()
    fun onCancel()
}