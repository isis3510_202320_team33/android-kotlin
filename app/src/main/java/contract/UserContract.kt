package contract

import model.entity.UserEntity

interface UserContract {


    interface viewUser{
        fun showConnection()
        fun showError(message: String)
        fun authSuccess(user: UserEntity)

    }

    interface presenterUser{
        suspend fun authUser(email: String, password: String)

    }


}