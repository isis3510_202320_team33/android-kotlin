package contract

import android.content.Context
import model.entity.ApartmentEntity

interface DashboardContract {

    interface view{

        fun showConnection()
        fun showError(message: String)
        fun insertValues(has1: HashMap<String, List<String>>, has2: HashMap<String, List<String>>)
    }

    interface presenter{
        suspend fun getApartments(context: Context, id: String)
    }

}