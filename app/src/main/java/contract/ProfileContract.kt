package contract

import android.content.Context
import model.entity.UserEntity

interface ProfileContract {

    interface view{

        fun showConnection()
        fun showError(message: String)
        fun insertValues(user: UserEntity)
    }

    interface presenter{
        suspend fun getProfile(email: String, password: String)
    }
}