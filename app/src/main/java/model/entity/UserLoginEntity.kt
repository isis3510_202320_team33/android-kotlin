package model.entity

data class UserLoginEntity(

    val email: String,
    val password: String,

)
