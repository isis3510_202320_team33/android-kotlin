package model.entity

import java.io.Serializable

data class ApartmentEntity(
    
    val name: String = "",
    val city: String = "",
    val neighborhood: String = "",
    val address: String = "",
    val housingType: String = "", /*Apartment or House*/
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val rentPrice: Long = 0,
    val stratum: Int = -1,
    val area: Double = 0.0,
    val apartmentFloor: Int = 0,
    val roomsNumber: Int = 0,
    val availableRoom: Int = 0,
    val roomArea: Double = 0.0,
    val bathroomsNumber: Int = 0,
    val laundryArea: Boolean = false,
    val internet: Boolean = false,
    val tv: Boolean = false,
    val furnished: Boolean = false,
    val elevator: Boolean = false,
    val smoke: Boolean = false,
    val vape: Boolean = false,
    val pets: Boolean = false,
    val gymnasium: Boolean = false,
    val supermarkets: Boolean = false,
    val reception: Boolean = false,
    val rating: Int = 0,
    val description: String = "",
    var images: MutableList<String> = ArrayList(),
    var idUser: String = ""
) : Serializable
