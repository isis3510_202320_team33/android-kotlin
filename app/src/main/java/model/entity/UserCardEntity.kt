package model.entity

import android.graphics.Bitmap

data class UserCardEntity(

    var id: String = "",
    var full_name: String = "",
    var stars: Int = 0,
    var image: String =  "",
    var imageDos: Bitmap? = null
    )