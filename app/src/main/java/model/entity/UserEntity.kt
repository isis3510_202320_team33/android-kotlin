package model.entity


data class UserEntity(
    var id: String = "",
    val full_name: String = "",
    val email: String = "",
    var password: String = "",
    val age: Int = 0,
    val phone: Long = 0,
    val gender: String = "",
    val city: String = "",
    val locality: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val bring_people: String = "",
    val sleep: Int = 0,
    val rol: String = "",
    val clean: String = "",
    val stars: Int = 0,
    val vape: Boolean = false,
    val personality: String = "",
    val likes_pets: Boolean = false,
    val smoke: Boolean = false,
    var image: String =  "",
)
