package model.dao

import android.graphics.Bitmap
import model.entity.UserEntity

interface UserDAO {

    suspend fun authCredentials(email: String): UserEntity

    suspend fun insertUser(user: UserEntity, image: Bitmap):String?

}