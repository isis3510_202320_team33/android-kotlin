    package model.dao

    import android.graphics.Bitmap
    import model.entity.ApartmentEntity

    interface ApartmentDAO {

        suspend fun insertApartment(apartment: ApartmentEntity, photos: List<Bitmap>, user: String):String?

        suspend fun getApartmentsByUser(idUser: String): List<ApartmentEntity>?

        /*
        fun updateApartment(apartment: ApartmentEntity)
        fun deleteApartment(apartmentId: Int)*/
        suspend fun getAllApartments(): ArrayList<ApartmentEntity>?

    }