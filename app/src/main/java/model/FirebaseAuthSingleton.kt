package model

import com.google.firebase.auth.FirebaseAuth

object FirebaseAuthSingleton {
    private var instance: FirebaseAuth? = null

    fun getInstance(): FirebaseAuth {
        if (instance == null) {
            instance = FirebaseAuth.getInstance()
        }
        return instance!!
    }
}
