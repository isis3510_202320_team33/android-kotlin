
import android.graphics.Bitmap
import android.util.Log
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await
import model.FirebaseStorageSingleton
import model.dao.ApartmentDAO
import model.entity.ApartmentEntity
import model.entity.UserEntity
import java.io.ByteArrayOutputStream

class ApartmentDAOImplement : ApartmentDAO {

    private val firestore = FirebaseDataBaseSingleton.firestore
    private val apartmentsCollection = firestore.collection("Houses")
    private val firestorage =  FirebaseStorageSingleton.getInstance()
    private val storageReference = firestorage.reference.child("images_houses")

    override suspend fun insertApartment(apartment: ApartmentEntity, photos: List<Bitmap>, user: String): String? {
        val imageUrls = mutableListOf<String>()

        try {

            var i = 0
            while (i < photos.size)
            {
                val nameFolderApartment =  "${apartment.name}_${apartment.city}_${apartment.neighborhood}_${apartment.address}"
                val folderApartment =  storageReference.child(nameFolderApartment)
                val imageRef = folderApartment.child("imagen$i.jpg")
                val byteArrayOutputStream = ByteArrayOutputStream()
                photos[i].compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                val bytes = byteArrayOutputStream.toByteArray()
                val imageUrl = uploadImageAndGetUrl(imageRef, bytes)
                imageUrls.add(imageUrl)
                i++
            }

            apartment.images = imageUrls
            apartment.idUser =  user
            return saveApartmentToFirestore(apartment)
        } catch (e: Exception) {
            Log.e("Valores", "Error en la transacción", e)
            return null
        }

    }

    private  fun saveApartmentToFirestore(apartment: ApartmentEntity): String? {
        return try {
            val apartmentDocRef = apartmentsCollection.document()
            apartmentDocRef.set(apartment)
            Log.d("Valores", "DocumentSnapshot written with ID: ${apartmentDocRef.id}")
            apartmentDocRef.id
        } catch (e: Exception) {
            Log.e("Valores", "Error saving apartment", e)
            return null
        }
    }

    private suspend fun uploadImageAndGetUrl(imageRef: StorageReference, bytes: ByteArray): String {
        return try {
            val uploadTask = imageRef.putBytes(bytes)
            val taskSnapshot = uploadTask.await()
            val urlImage = imageRef.downloadUrl.await()
            Log.d("Valores", "INsertion images: ${urlImage}")
            return urlImage.toString()
        } catch (e: Exception) {
            Log.e("Valores", "Error saving photo", e)
            throw e
        }
    }


    override suspend fun getApartmentsByUser(email: String): List<ApartmentEntity>? {
        return try {
            val userGet = apartmentsCollection.whereEqualTo("idUser", email).get().await()
            val apartmentList = mutableListOf<ApartmentEntity>()

            for (document in userGet.documents) {
                val apartmentEntity = document.toObject(ApartmentEntity::class.java)
                if (apartmentEntity != null) {
                    apartmentList.add(apartmentEntity)
                } else {
                    throw Exception("No se pudo convertir el documento a ApartmentEntity")
                }
            }

            return apartmentList

        } catch (e: Exception) {
            Log.e("Valores", "Error getting apartment", e)
            return null
        }
    }

    override suspend fun getAllApartments(): ArrayList<ApartmentEntity>? {
        return try {
            val collectionList = apartmentsCollection.get().await()
            val apartmentList = ArrayList<ApartmentEntity>()
            for (document in collectionList) {
                val apartmentEntity = document.toObject(ApartmentEntity::class.java)
                if (apartmentEntity != null) {
                    apartmentList.add(apartmentEntity)
                } else {
                    throw Exception("No se pudo convertir el documento a ApartmentEntity")
                }
            }
            return apartmentList

        } catch (e: Exception) {
            Log.e("Valores", "Error getting apartment", e)
            return null
    }
    }
}
