package model.daoImplement

import android.graphics.Bitmap
import android.util.Log
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await
import model.FirebaseStorageSingleton
import model.dao.UserDAO
import model.entity.UserEntity
import java.io.ByteArrayOutputStream

class UserDAOImplement : UserDAO {

    private val firestore = FirebaseDataBaseSingleton.firestore
    private val usersCollection = firestore.collection("Users")

    private val firestorage =  FirebaseStorageSingleton.getInstance()
    private val storageReference = firestorage.reference.child("images_profile")


    override suspend fun authCredentials(email: String): UserEntity {
        return try {
            val userGet = usersCollection.whereEqualTo("email", email).get().await()
            if (!userGet.isEmpty) {
                val documentSnapshot = userGet.documents[0]
                val userEntity = documentSnapshot.toObject(UserEntity::class.java)
                if (userEntity != null) {
                    return userEntity
                } else {
                    throw Exception("No se pudo convertir el documento a UserEntity")
                }
            } else {
                throw Exception("No se encontró ningún usuario con el correo electrónico proporcionado")
            }

        } catch (e: Exception) {
            throw e
        }
    }


    override suspend fun insertUser(user: UserEntity, image: Bitmap): String? {

        return try {

            val nameUser =  "${user.full_name}_${user.phone}"
            val imageRef =  storageReference.child(nameUser)
            val byteArrayOutputStream = ByteArrayOutputStream()
            image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            val bytes = byteArrayOutputStream.toByteArray()
            val imageUrl = uploadImageAndGetUrl(imageRef, bytes)

            user.image = imageUrl
            saveUserToFirestore(user)
        } catch (e: Exception) {
            Log.e("Valores", "Error en la transacción", e)
            null
        }

    }

    private fun saveUserToFirestore(user: UserEntity): String {
        return try {
            val userDocRef = usersCollection.document()
            userDocRef.set(user)
            Log.d("Valores", "User written with ID: ${userDocRef.id}")
            userDocRef.id
        } catch (e: Exception) {
            throw e
        }
    }

    private suspend fun uploadImageAndGetUrl(imageRef: StorageReference, bytes: ByteArray): String {
        try {
            val uploadTask = imageRef.putBytes(bytes)
            val taskSnapshot = uploadTask.await()
            val urlImage = imageRef.downloadUrl.await()
            Log.d("Valores", "Insertion image user: ${urlImage}")
            return urlImage.toString()
        } catch (e: Exception) {
            throw e
        }
    }

}
