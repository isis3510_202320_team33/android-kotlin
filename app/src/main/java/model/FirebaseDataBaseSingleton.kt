import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FirebaseFirestore
import android.content.Context
object FirebaseDataBaseSingleton {
    private var initialized = false
    lateinit var firestore: FirebaseFirestore

    fun init(context: Context) {
        if (!initialized) {
            FirebaseApp.initializeApp(context)
            firestore = FirebaseFirestore.getInstance()
            initialized = true
        }
    }

     fun getInit(): Boolean
    {
        return initialized
    }
}
