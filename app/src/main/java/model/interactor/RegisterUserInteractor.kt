package model.interactor


import android.graphics.Bitmap
import android.util.Log
import model.entity.UserEntity
import model.repository.RegisterUserRepository
import model.repository.remote.RegisterUserRemote

class RegisterUserInteractor {

    private val registerUserRepository: RegisterUserRepository
    init {
        registerUserRepository = RegisterUserRepository(RegisterUserRemote() )
    }

     suspend fun registerUser( user: UserEntity, image: Bitmap): String? {

         val searchUserByEmial = registerUserRepository.searchUser(user.email)
         return if (searchUserByEmial == null) {
             Log.d("Valores","estamoes en el interactor de register user")
             registerUserRepository.registerUser( user, image)

         } else{
             "usuario existente"
         }

    }


}