package model.interactor

import android.graphics.Bitmap
import model.entity.ApartmentEntity
import model.repository.ApartmentRepository
import model.repository.remote.ApartmentRemote
import model.utils.Connectivity
import utils.ContextApplication

class ApartmentInteractor {

    private val apartmentRepository: ApartmentRepository = ApartmentRepository(ApartmentRemote() )
    private lateinit var connectivity: Connectivity

    suspend fun insertApartment(apartmentEntity: ApartmentEntity, photos: List<Bitmap>, user: String): String? {

        connectivity = Connectivity(ContextApplication.appContext)
        val connection = connectivity.isNetworkAvailable()
        try {
            if (!connection) {
                return "No connection"
            } else if (connection) {
                return apartmentRepository.insertApartment(apartmentEntity, photos, user)
            }
        } catch (e: Exception) {

            return null
        }
        return null
    }

}