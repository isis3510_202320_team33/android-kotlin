package model.interactor

import model.entity.UserEntity
import model.repository.UserRepository
import model.repository.remote.UserRemote
import model.utils.Connectivity
import utils.ContextApplication

class UserInteractor {

    private val userRepository: UserRepository = UserRepository( UserRemote())
    private lateinit var connectivity: Connectivity
    suspend fun sendDataUser(email: String, password: String): UserEntity?
    {
        connectivity = Connectivity(ContextApplication.appContext)
        val connection = connectivity.isNetworkAvailable()
        return try {
            if (connection) {
                userRepository.sendDataUser(email, password)
            } else {
                UserEntity(full_name = "No")
            }
        } catch (e: Exception) {
            null
        }
    }
}