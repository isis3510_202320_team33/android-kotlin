package model.interactor

import android.content.Context
import model.repository.DashboardRepository
import model.repository.local.LocalDashboard
import model.repository.model.Metric
import model.repository.remote.RemoteDashboard
import model.utils.Connectivity


class DashboardInteractor {

    private  val dashboardRepository: DashboardRepository = DashboardRepository(LocalDashboard(), RemoteDashboard() )
    private lateinit var connectivity: Connectivity

    suspend fun getApartments(context: Context, id: String): Metric? {
        connectivity = Connectivity(context)
        val connection = connectivity.isNetworkAvailable()
        try {
            if (!connection) {
                val data = dashboardRepository.getApartmentsCache(id)
                return if (data != null) {
                    data
                } else {
                    null
                }
            }
            else {
                return dashboardRepository.getApartmentsRemote(id)
            }
        } catch (e: Exception) {
            return null
        }
    }
}