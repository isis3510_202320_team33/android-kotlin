package model.interactor

import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import model.entity.UserCardEntity
import model.entity.UserEntity
import model.repository.ApartmentRepository
import model.repository.ListUserRepository
import model.repository.local.ListUserLocal
import model.repository.remote.ApartmentRemote
import model.repository.remote.ListUserRemote
import model.utils.Connectivity
import utils.ContextApplication

class ListUserInteractor {

    private val listUserRepository: ListUserRepository

    private lateinit var connectivity: Connectivity
    init {
        listUserRepository = ListUserRepository(ListUserRemote(), ListUserLocal(ContextApplication.appContext) )
    }

    suspend fun getAllUsers(pageSize: Long, lastVisibleDocument: DocumentSnapshot?):
            Triple<ArrayList<UserCardEntity>?, DocumentSnapshot?, String>
    {
        return try {
            listUserRepository.getAllUsers(pageSize, lastVisibleDocument)
        } catch (e: Exception) {
            Log.e("Valores", "Error en el interactor", e)
            Triple(null, null, "")
        }

    }


     fun getAllUsersCache(): ArrayList<UserCardEntity>{
         try {
             val lista = ArrayList<UserCardEntity>()
             val userList = listUserRepository.getAllUserCache()
             for (element in userList) {
                 val image = listUserRepository.getImage(element.id)
                 Log.d("Valores", "imagen Bitmap obtenida: $image")
                 element.imageDos = image
                 Log.d("Valores", "imagen Bitmap asignada a user: ${element.imageDos}")
                 lista.add(element)
             }
             return lista
         } catch (e: Exception) {
             Log.e("Valores", "Error en el interactor", e)
             return ArrayList()
         }
    }

    fun sizeCache(): Int{
        return listUserRepository.getSizeCache()
    }


}