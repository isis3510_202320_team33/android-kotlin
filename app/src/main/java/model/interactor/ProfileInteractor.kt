package model.interactor

import android.util.Log
import model.entity.UserEntity
import model.repository.ProfileRepository
import model.repository.UserRepository
import model.repository.local.ProfileLocal
import model.repository.remote.ProfileRemote
import model.repository.remote.UserRemote
import model.utils.Connectivity
import utils.ContextApplication

class ProfileInteractor {

    private val profileRepository: ProfileRepository

    init {
        profileRepository = ProfileRepository( ProfileRemote(), ProfileLocal())
    }

    suspend fun getUser(email: String, password: String): UserEntity?
    {

        return profileRepository.getUser(email, password)

    }

}