package model
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage

object FirebaseStorageSingleton {
    private var storageInstance: FirebaseStorage? = null

    fun getInstance(): FirebaseStorage {
        if (storageInstance == null) {
            storageInstance = Firebase.storage
        }
        return storageInstance!!
    }
}
