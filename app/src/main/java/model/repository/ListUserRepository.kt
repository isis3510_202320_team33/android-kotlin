package model.repository

import android.graphics.Bitmap
import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import model.entity.UserCardEntity
import model.repository.Irepository.IListUserRepository

class ListUserRepository(private val remoteRepository: IListUserRepository,
                            private val localRepository: IListUserRepository): IListUserRepository {

    override suspend fun getAllUsers(pageSize: Long, lastVisibleDocument: DocumentSnapshot?):
            Triple<ArrayList<UserCardEntity>?, DocumentSnapshot?, String>  {
        val userList = remoteRepository.getAllUsers(pageSize, lastVisibleDocument)
        for (element in userList.first!!) {
            val user = UserCardEntity(
                id = element.id,
                full_name = element.full_name,
                stars = element.stars,
                image = "",
                imageDos = null,
            )
            Log.d("Valores", "En repositorio, almacenando datos$user")
            localRepository.putUserList(element.id, user)
            localRepository.putImage(element.id, element.image )
        }
        return userList
    }

    override fun getSizeCache(): Int {
        return localRepository.getSizeCache()
    }

    override fun getAllUserCache(): ArrayList<UserCardEntity> {
       return localRepository.getAllUserCache()
    }

    override fun putUserList(key: String, value: UserCardEntity) {
        TODO("Not yet implemented")
    }

    override fun getImage(key: String): Bitmap {
        return localRepository.getImage(key)
    }

    override fun putImage(key: String, url: String) {
        TODO("Not yet implemented")
    }
}