package model.repository

import android.graphics.Bitmap
import model.entity.ApartmentEntity
import model.repository.Irepository.IApartmentRepository

class ApartmentRepository(private val remoteRepository: IApartmentRepository):
    IApartmentRepository {

    override suspend fun insertApartment(apartmentEntity: ApartmentEntity, photos: List<Bitmap>, user: String): String?
    {
        return remoteRepository.insertApartment(apartmentEntity, photos, user)
    }

    override suspend fun getAllApartments(): ArrayList<ApartmentEntity>?{
        return remoteRepository.getAllApartments()
    }

}