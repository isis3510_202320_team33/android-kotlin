package model.repository

import android.graphics.Bitmap
import android.util.Log
import model.entity.UserEntity
import model.repository.Irepository.IProfileRepository
import model.repository.Irepository.IUserRepository
import model.utils.Connectivity
import utils.ContextApplication

class ProfileRepository(private val remoteRepository: IProfileRepository,
                        private val localRepository: IProfileRepository): IProfileRepository {

    private lateinit var connectivity: Connectivity
    override suspend fun getUser(email: String, password: String): UserEntity? {
        try {

            val respuestaRoom = localRepository.getUser(email, password)
            if (respuestaRoom != null)
            {
                return respuestaRoom
            }

            else
            {

                connectivity = Connectivity(ContextApplication.appContext)
                val connection = connectivity.isNetworkAvailable()
                Log.d("Valores", "answer connection: $connection")
                if (connection) {
                    Log.d("Valores", "Hay conexión")
                    //Hay conexión
                    Log.d("Valores", "Buscando user...")
                    val user = remoteRepository.authCredentials(email)
                    Log.d("Valores", "Usuario obtenido... $user")
                    val passwordHash =  user.password
                    val respuesta = passwordEqual(password, passwordHash)
                    Log.d("Valores", "Comparar passwords... $respuesta")
                    return if (respuesta) {
                        val registerUser = localRepository.registerUser(user)
                        Log.d("Valores", "User guardado en el Room...$registerUser")
                        user
                    }else {
                        Log.d("Valores", "Las contraseñas no coinciden...")
                        null
                    }
                }
                else
                {
                    return UserEntity(full_name = "No")
                }
            }

        }catch (e: Exception)
        {
            Log.e("Valores", "Error al comparar las contraseñas", e)
            return null
        }
    }

    override suspend fun authCredentials(email: String): UserEntity {
        TODO("Not yet implemented")
    }

    override suspend fun registerUser(user: UserEntity): String? {
        TODO("Not yet implemented")
    }

    private fun passwordEqual(passwordLocal: String, passwordRemote: String): Boolean
    {
        if (passwordRemote == passwordLocal)
        {
            return true
        }
        return false
    }

}