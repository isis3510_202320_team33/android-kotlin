package model.repository.remote

import android.graphics.Bitmap
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.tasks.await
import model.FirebaseStorageSingleton
import model.entity.ApartmentEntity
import model.repository.Irepository.IApartmentRepository
import java.io.ByteArrayOutputStream

class ApartmentRemote: IApartmentRepository {

    private val firestore = FirebaseDataBaseSingleton.firestore
    private val apartmentsCollection = firestore.collection("Houses")
    private val firestorage =  FirebaseStorageSingleton.getInstance()
    private val storageReference = firestorage.reference.child("images_houses")

    override suspend fun insertApartment(apartment: ApartmentEntity, photos: List<Bitmap>, user: String): String? {
        val imageUrls = mutableListOf<String>()

        try {

            var i = 0
            while (i < photos.size)
            {
                val nameFolderApartment =  "${apartment.name}_${apartment.city}_${apartment.neighborhood}_${apartment.address}"
                val folderApartment =  storageReference.child(nameFolderApartment)
                val imageRef = folderApartment.child("imagen$i.jpg")
                val byteArrayOutputStream = ByteArrayOutputStream()
                photos[i].compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                val bytes = byteArrayOutputStream.toByteArray()
                val imageUrl = uploadImageAndGetUrl(imageRef, bytes)
                imageUrls.add(imageUrl)
                i++
            }

            apartment.images = imageUrls
            apartment.idUser =  user
            return saveApartmentToFirestore(apartment)
        } catch (e: Exception) {
            return null
        }

    }

    private  fun saveApartmentToFirestore(apartment: ApartmentEntity): String? {
        return try {
            val apartmentDocRef = apartmentsCollection.document()
            apartmentDocRef.set(apartment)
            apartmentDocRef.id
        } catch (e: Exception) {
            return null
        }
    }

    private suspend fun uploadImageAndGetUrl(imageRef: StorageReference, bytes: ByteArray): String {
        return try {
            val uploadTask = imageRef.putBytes(bytes)
            uploadTask.await()
            val urlImage = imageRef.downloadUrl.await()
            return urlImage.toString()
        } catch (e: Exception) {
            throw e
        }
    }


    override suspend fun getAllApartments(): ArrayList<ApartmentEntity>? {
        return try {
            val collectionList = apartmentsCollection.get().await()
            val apartmentList = ArrayList<ApartmentEntity>()
            for (document in collectionList) {
                val apartmentEntity = document.toObject(ApartmentEntity::class.java)
                if (apartmentEntity != null) {
                    apartmentList.add(apartmentEntity)
                } else {
                    throw Exception("No se pudo convertir el documento a ApartmentEntity")
                }
            }
            return apartmentList

        } catch (e: Exception) {
            return null
        }
    }

}