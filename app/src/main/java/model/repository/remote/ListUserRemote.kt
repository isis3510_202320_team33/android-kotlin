package model.repository.remote

import android.graphics.Bitmap
import android.util.Log
import com.google.firebase.firestore.DocumentSnapshot
import kotlinx.coroutines.tasks.await
import model.entity.ApartmentEntity
import model.entity.UserCardEntity
import model.entity.UserEntity
import model.repository.Irepository.IListUserRepository

class ListUserRemote: IListUserRepository {

    private val firestore = FirebaseDataBaseSingleton.firestore
    private val usersCollection = firestore.collection("Users")

    override suspend fun getAllUsers(pageSize: Long, lastVisibleDocument: DocumentSnapshot?): Triple<ArrayList<UserCardEntity>?, DocumentSnapshot?, String> {
        return try {

            Log.d("Valores", "page size: $pageSize")
            Log.d("Valores", "last document: $lastVisibleDocument")
            val query = if (lastVisibleDocument == null) {
                usersCollection.limit(pageSize)
            } else {
                usersCollection.startAfter(lastVisibleDocument).limit(pageSize)
            }

            val collectionList = query.get().await()

            val userList = ArrayList<UserCardEntity>()

            for (document in collectionList) {
                val userEntity = document.toObject(UserCardEntity::class.java)
                if (userEntity != null) {
                    userEntity.id = document.id
                    userEntity.full_name = document.getString("full_name") ?: ""
                    userEntity.stars = (document.getLong("stars") ?: 0).toInt()
                    userEntity.image = document.getString("image") ?: ""
                    userEntity.imageDos = null
                    userList.add(userEntity)
                } else {
                    throw Exception("No se pudo convertir el documento a UserEntity")
                }
            }

            val lastVisible = if (userList.isNotEmpty()) collectionList.documents.last() else null
            Log.d("Valores", "last document select: $lastVisible")
            Triple(userList, lastVisible, "")

        } catch (e: Exception) {
            Log.e("Valores", "Error getting users", e)
            Triple(null, null, "")
        }
    }

    override fun getSizeCache(): Int {
        TODO("Not yet implemented")
    }

    override fun getAllUserCache(): ArrayList<UserCardEntity> {
        TODO("Not yet implemented")
    }

    override fun putUserList(key: String, value: UserCardEntity) {
        TODO("Not yet implemented")
    }

    override fun getImage(key: String): Bitmap {
        TODO("Not yet implemented")
    }

    override fun putImage(key: String, url: String) {
        TODO("Not yet implemented")
    }
}