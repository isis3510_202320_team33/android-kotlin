package model.repository.remote

import model.repository.IApi.ApiDashboard
import model.repository.Irepository.IDashboardRepository
import model.repository.model.Metric
import org.json.JSONObject
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RemoteDashboard: IDashboardRepository {

    private val baseURL = "http://34.123.214.132:8000/senehouse/"
    private val apiService: ApiDashboard

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl(baseURL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        apiService = retrofit.create(ApiDashboard::class.java)
    }

    override suspend fun getApartmentsRemote(id: String): Metric?{
         try {
            val response = apiService.getMetrics(id)
            if (response.isSuccessful) {
                val responseBody = response.body()
                if (responseBody != null) {
                    val respuesta = convertToModel(responseBody)
                    return respuesta
                }
            }
        } catch (e: Exception) {
            return null
        }

       return null
    }
    
    private fun convertToModel(response: String): Metric
    {
        val json = JSONObject(response)
        val occupancyRateJson = json.getJSONObject("occupancyRate")
        val revenueJson = json.getJSONObject("revenue")

        val occupancyRateMap = HashMap<String, List<String>>()
        val revenueMap = HashMap<String, List<String>>()

        for (key in occupancyRateJson.keys()) {
            val jsonArray = occupancyRateJson.getJSONArray(key)
            val stringList = ArrayList<String>()

            for (i in 0 until jsonArray.length()) {
                stringList.add(jsonArray.getString(i))
            }

            occupancyRateMap[key] = stringList
        }

        for (key in revenueJson.keys()) {
            val jsonArray = revenueJson.getJSONArray(key)
            val stringList = ArrayList<String>()

            for (i in 0 until jsonArray.length()) {
                stringList.add(jsonArray.getString(i))
            }

            revenueMap[key] = stringList
        }

        return Metric(occupancyRateMap, revenueMap)
    }

    override suspend fun addOrUpdateMetric(email: String, metric: Metric?) {
        TODO("Not yet implemented")
    }


    override suspend fun removeMetric(email: String) {
        TODO("Not yet implemented")
    }

    override suspend fun getApartmentsCache(email: String): Metric? {
        TODO("Not yet implemented")
    }

}