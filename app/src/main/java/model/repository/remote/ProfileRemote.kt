package model.repository.remote

import android.graphics.Bitmap
import kotlinx.coroutines.tasks.await
import model.entity.UserEntity
import model.repository.Irepository.IProfileRepository

class ProfileRemote: IProfileRepository {


    private val firestore = FirebaseDataBaseSingleton.firestore
    private val usersCollection = firestore.collection("Users")

    override suspend fun authCredentials(email: String): UserEntity {
        return try {
            val userGet = usersCollection.whereEqualTo("email", email).get().await()
            if (!userGet.isEmpty) {
                val documentSnapshot = userGet.documents[0]
                val userEntity = documentSnapshot.toObject(UserEntity::class.java)
                if (userEntity != null) {
                    userEntity.id = documentSnapshot.id
                    return userEntity
                } else {
                    throw Exception("No se pudo convertir el documento a UserEntity")
                }
            } else {
                throw Exception("No se encontró ningún usuario con el correo electrónico proporcionado")
            }

        } catch (e: Exception) {
            throw e
        }
    }


    override suspend fun getUser(email: String, password: String): UserEntity? {
        TODO("Not yet implemented")
    }

    override suspend fun registerUser( user: UserEntity): String?
    {
        TODO("Not yet implemented")
    }

}