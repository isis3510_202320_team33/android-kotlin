package model.repository.IApi
import model.HouseDTO.HouseDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiApartmentOffers {
    @GET("nearestoffers")
    suspend fun getNearestOffers(@Query("maxDistance") maxDistance: Int?, @Query("longitude") longitude: Double, @Query("latitude") latitude: Double): List<HouseDTO>
}