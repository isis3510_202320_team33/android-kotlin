package model.repository.IApi

import retrofit2.http.GET

interface ApiDescriptions {
    @GET("bestdescriptions")
    suspend fun getBestDescriptions(): List<String>
}