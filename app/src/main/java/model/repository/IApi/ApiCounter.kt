package model.repository.IApi

import retrofit2.Call
import retrofit2.http.PUT
import retrofit2.http.Path

interface ApiCounter {
    @PUT("houses/{house_id}/views")
    suspend fun updateCounter(@Path("house_id") houseId: String): Call<Unit>
}