package model.repository.IApi

import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.Response

interface ApiDashboard {

    @GET("houses/{id}")
    suspend fun getMetrics(@Path("id") id: String): Response<String>

}