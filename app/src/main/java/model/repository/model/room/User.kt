package model.repository.model.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @PrimaryKey
    val id: String,
    @ColumnInfo(name = "full_name") val fullName: String,
    val email: String,
    var password: String,
    val age: Int,
    val phone: Long,
    val gender: String,
    val city: String,
    val locality: String,
    val latitude: Double,
    val longitude: Double,
    @ColumnInfo(name = "bring_people") val bringPeople: String,
    val sleep: Int,
    val rol: String,
    val clean: String,
    val stars: Int,
    val vape: Boolean,
    val personality: String,
    @ColumnInfo(name = "likes_pets") val likesPets: Boolean,
    val smoke: Boolean,
    var image: String
)

