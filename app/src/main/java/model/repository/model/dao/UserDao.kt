package model.repository.model.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import model.repository.model.room.User

@Dao
interface UserDao {
    @Insert
    suspend fun insertUser(user: User)
    @Query("SELECT * FROM user")
    suspend fun getUser(): User
    @Query("DELETE FROM user")
    fun deleteExpiredEntity()
}