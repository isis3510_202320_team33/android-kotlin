package model.repository.model

data class Metric(
    val occupancyRate: HashMap<String, List<String>>,
    val revenue: HashMap<String, List<String>>
)
