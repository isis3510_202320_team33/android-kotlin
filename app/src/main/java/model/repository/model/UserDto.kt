package model.repository.model

import model.entity.UserEntity
import model.repository.model.room.User

object UserDto {

    fun toUserEntity(user: User): UserEntity {
        return UserEntity(
            id = user.id,
            full_name = user.fullName,
            email = user.email,
            password = user.password,
            age = user.age,
            phone = user.phone,
            gender = user.gender,
            city = user.city,
            locality = user.locality,
            latitude = user.latitude,
            longitude = user.longitude,
            bring_people = user.bringPeople,
            sleep = user.sleep,
            rol = user.rol,
            clean = user.clean,
            stars = user.stars,
            vape = user.vape,
            personality = user.personality,
            likes_pets = user.likesPets,
            smoke = user.smoke,
            image = user.image
        )
    }

    fun toUser(userEntity: UserEntity): User {
        return User(
            id = userEntity.id,
            fullName = userEntity.full_name,
            email = userEntity.email,
            password = userEntity.password,
            age = userEntity.age,
            phone = userEntity.phone,
            gender = userEntity.gender,
            city = userEntity.city,
            locality = userEntity.locality,
            latitude = userEntity.latitude,
            longitude = userEntity.longitude,
            bringPeople = userEntity.bring_people,
            sleep = userEntity.sleep,
            rol = userEntity.rol,
            clean = userEntity.clean,
            stars = userEntity.stars,
            vape = userEntity.vape,
            personality = userEntity.personality,
            likesPets = userEntity.likes_pets,
            smoke = userEntity.smoke,
            image = userEntity.image
        )
    }
}
