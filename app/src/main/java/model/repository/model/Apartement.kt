package model.repository.model

data class Apartement(
    val roomsNumber: Int = 0,
    val apartmentFloor: Int = 0,
    val description: String = "",
    val gymnasium: Boolean = false,
    val longitude: Double = 0.0,
    val tv: Boolean = false,
    val reception: Boolean = false,
    val rating: Int = 0,
    val roomArea: Double = 0.0,
    val housingType: String = "", /*Apartment or House*/
    val rentPrice: Long = 0,
    val city: String = "",
    val neighborhood: String = "",
    val area: Double = 0.0,
    val stratum: Int = -1,
    val elevator: Boolean = false,
    val laundryArea: Boolean = false,
    val name: String = "",
    val bathroomsNumber: Int = 0,
    val internet: Boolean = false,
    val address: String = "",
    val latitude: Double = 0.0,
    val furnished: Boolean = false,
    val supermarkets: Boolean = false
)