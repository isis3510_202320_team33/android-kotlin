package model.repository

import model.repository.Irepository.IDashboardRepository
import model.repository.model.Metric

class DashboardRepository(private val localRepository: IDashboardRepository,
                          private val remoteRepository: IDashboardRepository ): IDashboardRepository {




    override suspend fun getApartmentsRemote(id: String): Metric? {

        val datas = remoteRepository.getApartmentsRemote(id)
        localRepository.addOrUpdateMetric(id, datas)
        return datas

    }

    override suspend fun getApartmentsCache(id: String): Metric? {
        return localRepository.getApartmentsCache(id)

    }

    override suspend fun addOrUpdateMetric(email: String, metric: Metric?) {
        TODO("Not yet implemented")
    }

    override suspend fun removeMetric(email: String) {
        TODO("Not yet implemented")
    }


}