package model.repository

import org.mindrot.jbcrypt.BCrypt
import android.graphics.Bitmap
import android.util.Log
import kotlinx.coroutines.tasks.await
import model.entity.UserEntity
import model.repository.Irepository.IRegisterUserRepository
import model.utils.Connectivity
import utils.ContextApplication

class RegisterUserRepository(private val remoteRepository: IRegisterUserRepository):  IRegisterUserRepository
{
    private lateinit var connectivity: Connectivity

    override suspend fun registerUser( user: UserEntity, image: Bitmap): String? {

        connectivity = Connectivity(ContextApplication.appContext)
        val connection = connectivity.isNetworkAvailable()

        Log.d("Valores", "answer connection: $connection")
        try {
            val password =  user.password
            user.password = hashPassword(password)
            if (!connection) {
                Log.d("Valores", "No hay conexión")
                //No hay conexión
                return "No connection"
            } else if (connection) {
                Log.d("Valores", "Hay conexión")
                //Hay conexión
                return remoteRepository.registerUser(user, image)
            }
        } catch (e: Exception) {
            Log.e("Valores", "Error en el interactor", e)
        }
        return null
    }


    private fun hashPassword(password: String): String {
        return BCrypt.hashpw(password, BCrypt.gensalt())
    }

    override suspend fun searchUser(email: String): UserEntity?
    {
        val answer =  remoteRepository.searchUser(email)
        return if (answer == null) {
            null
        } else {
            remoteRepository.searchUser(email)
        }

    }

}