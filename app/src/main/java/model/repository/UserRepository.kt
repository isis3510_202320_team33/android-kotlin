package model.repository

import model.entity.UserEntity
import model.repository.Irepository.IUserRepository
import org.mindrot.jbcrypt.BCrypt

class UserRepository(private val remoteRepository: IUserRepository):  IUserRepository  {
    override suspend fun sendDataUser(email: String, password: String): UserEntity?
    {
        return try {
            val user = remoteRepository.authCredentials(email)
            val passwordHash =  user.password
            val respuesta = verifyPassword(password, passwordHash)
            if (respuesta) {
                user
            }else {
                null
            }
        }catch (e: Exception) {
            null
        }
    }

    private fun verifyPassword(password: String, hashedPassword: String): Boolean {
        return  BCrypt.checkpw(password, hashedPassword)
    }

    override suspend fun authCredentials(email: String): UserEntity {
        TODO("Not yet implemented")
    }

    override suspend fun getUser(email: String, password: String): UserEntity?
    {
        return try {
            val user = remoteRepository.authCredentials(email)
            val passwordHash =  user.password
            val respuesta = passwordEqual(password, passwordHash)
            if (respuesta) {
                user
            }else {
                null
            }
        }catch (e: Exception) {
            null
        }
    }

    private fun passwordEqual(passwordLocal: String, passwordRemote: String): Boolean
    {
        if (passwordRemote == passwordLocal)
        {
            return true
        }
        return false
    }

}