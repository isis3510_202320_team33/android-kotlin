package model.repository.local

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.util.LruCache
import com.bumptech.glide.Glide
import com.google.firebase.firestore.DocumentSnapshot
import model.entity.UserCardEntity
import model.repository.Irepository.IListUserRepository
import utils.ContextApplication

class ListUserLocal(context: Context): IListUserRepository {

    companion object{

        private var users: LruCache<String, UserCardEntity> = LruCache(20)
        private var userImages: LruCache<String, Bitmap> = LruCache(20)

        var instance: ListUserLocal? = null
        fun getInstance(context: Context) =
            instance ?: synchronized(this) {
                instance ?: ListUserLocal(context).also {
                    instance = it
                }
            }
    }

    override suspend fun getAllUsers(
        pageSize: Long,
        lastVisibleDocument: DocumentSnapshot?
    ): Triple<ArrayList<UserCardEntity>?, DocumentSnapshot?, String> {
        TODO("Not yet implemented")
    }

    override fun getSizeCache(): Int {
        TODO("Not yet implemented")
    }

    override fun getAllUserCache(): ArrayList<UserCardEntity> {
        val userEntities = ArrayList<UserCardEntity>()
        for (user in users.snapshot().values) {
            userEntities.add(user)
        }
        return userEntities
    }
    override fun putUserList(key: String, value: UserCardEntity) {
        if (users[key] == null){
            users.put(key, value)
        }
    }

    override fun getImage(key: String): Bitmap {
        return userImages.get(key)
    }

    override fun putImage(key: String, url: String) {
        if (userImages[key] == null)
        {
            try {
                val bitmap = Glide.with(ContextApplication.appContext)
                    .asBitmap()
                    .load(url)
                    .submit()
                    .get()

                userImages.put(key, bitmap)
                Log.d("Valores", "Imagen guardada")
            } catch (e: Exception) {
                Log.e("Valores", "error guardando image", e)
            }
        }
    }
}