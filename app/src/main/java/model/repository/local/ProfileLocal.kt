package model.repository.local

import android.graphics.Bitmap
import android.os.Environment
import android.util.Log
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import model.entity.UserEntity
import model.repository.Irepository.IProfileRepository
import model.repository.model.UserDto
import model.repository.model.db.AppDatabase
import model.repository.model.room.User
import utils.ContextApplication
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.UUID

class ProfileLocal: IProfileRepository {


    /*
    private val database: AppDatabase = Room.databaseBuilder(
        ContextApplication.appContext,
        AppDatabase::class.java,
        "app_database"
    ).build()*/


    override suspend fun getUser(email: String, password: String): UserEntity? {
        return withContext(Dispatchers.IO) {
            val userDao = AppDatabase.getDatabase(ContextApplication.appContext).userDao()
            val user = userDao.getUser()

            if (user != null) {
                val users = UserDto.toUserEntity(user)
                Log.d("Valores", "Usuario guardado en el room: $users")
                return@withContext users
            } else {
                Log.d("Valores", "El usuario es null")
                return@withContext null
            }
        }
    }


    override suspend fun registerUser(user: UserEntity): String? {
        return withContext(Dispatchers.IO) {
            val users = UserDto.toUser(user)
            val userDao = AppDatabase.getDatabase(ContextApplication.appContext).userDao()
            userDao.insertUser(users)
            val userRegister = userDao.getUser()
            Log.d("Valores", "Usuario guardado en el room: $userRegister")
            users.id
        }
    }

    override suspend fun authCredentials(email: String): UserEntity {
        TODO("Not yet implemented")
    }


}