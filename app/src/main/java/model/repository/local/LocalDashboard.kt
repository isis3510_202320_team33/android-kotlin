package model.repository.local

import android.util.ArrayMap
import model.repository.Irepository.IDashboardRepository
import model.repository.model.Metric

class LocalDashboard: IDashboardRepository{

    companion object {
        private var metrics: ArrayMap<String, Metric> = ArrayMap()
        @Volatile
        private var instance: LocalDashboard? = null
        fun getInstance(): LocalDashboard {
            return instance ?: synchronized(this) {
                instance ?: LocalDashboard().also { instance = it }
            }
        }
    }



    override suspend fun getApartmentsCache(id: String): Metric? {
        return metrics[id]
    }

    override suspend fun getApartmentsRemote(email: String): Metric?
    {
        TODO("Not yet implemented")
    }

    override suspend fun addOrUpdateMetric(email: String, metric: Metric?) {
        try {
            metrics[email] = metric
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override suspend fun removeMetric(email: String) {
        metrics.remove(email)
    }
}