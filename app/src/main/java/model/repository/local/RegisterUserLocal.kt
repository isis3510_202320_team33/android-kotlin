
package model.repository.local

import android.graphics.Bitmap
import android.os.Environment
import android.util.Log
import androidx.room.Room
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import model.entity.UserEntity
import model.repository.Irepository.IRegisterUserRepository
import model.repository.model.db.AppDatabase
import model.repository.model.room.User
import utils.ContextApplication
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.util.UUID


class RegisterUserLocal: IRegisterUserRepository{

    private val database: AppDatabase = Room.databaseBuilder(
        ContextApplication.appContext,
        AppDatabase::class.java,
        "app_database"
    ).build()


    override suspend fun registerUser( user: UserEntity, image: Bitmap): String? {
        return withContext(Dispatchers.IO) {
            val uniqueId = UUID.randomUUID().toString() // Generar un ID único
            val users = user.toUser().copy(id = uniqueId)
            val imageFile = saveBitmapToFile(image)
            users.image = imageFile?.absolutePath.toString()
            val userDao = database.userDao()
            userDao.insertUser(users)
            val gg = userDao.getUser()
            Log.d("Valores","Usuario guardado en el room$$gg")
            users.id
        }
    }


    private fun UserEntity.toUser(): User {
        return User(
            id = "",
            fullName = this.full_name,
            email = this.email,
            password = this.password,
            age = this.age,
            phone = this.phone,
            gender = this.gender,
            city = this.city,
            locality = this.locality,
            latitude = this.latitude,
            longitude = this.longitude,
            bringPeople = this.bring_people,
            sleep = this.sleep,
            rol = this.rol,
            clean = this.clean,
            stars = this.stars,
            vape = this.vape,
            personality = this.personality,
            likesPets = this.likes_pets,
            smoke = this.smoke,
            image = ""
        )
    }

    private fun saveBitmapToFile(bitmap: Bitmap): File? {
        val context = ContextApplication.appContext
        val imageFileName = "user_image.jpg"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        try {
            val imageFile = File(storageDir, imageFileName)
            val fos = FileOutputStream(imageFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.close()
            Log.d("Valores","Imagen guardada$fos")
            return imageFile
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }

    override suspend fun searchUser(email: String): UserEntity?
    {
        return UserEntity()
    }


}
