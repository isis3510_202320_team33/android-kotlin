package model.repository.Irepository

import android.graphics.Bitmap
import model.entity.UserEntity

interface IProfileRepository {

    suspend fun getUser(email: String, password: String): UserEntity?

    suspend fun authCredentials(email: String): UserEntity

    suspend fun registerUser( user: UserEntity): String?

}