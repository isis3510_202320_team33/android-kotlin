package model.repository.Irepository

import android.graphics.Bitmap
import model.entity.UserEntity

interface IRegisterUserRepository {

    suspend fun registerUser(user: UserEntity, image: Bitmap):String?

    suspend fun searchUser(email: String): UserEntity?

}