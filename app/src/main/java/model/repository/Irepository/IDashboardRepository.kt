package model.repository.Irepository

import model.repository.model.Metric

interface IDashboardRepository {

    suspend fun getApartmentsRemote(email: String): Metric?
    suspend fun getApartmentsCache(email: String): Metric?

    suspend fun addOrUpdateMetric(email: String, metric: Metric?)
    suspend fun removeMetric(email: String)

}