package model.repository.Irepository

import android.graphics.Bitmap
import model.entity.ApartmentEntity

interface IApartmentRepository {

    suspend fun insertApartment(apartment: ApartmentEntity, photos: List<Bitmap>, user: String): String?


    suspend fun  getAllApartments(): ArrayList<ApartmentEntity>?
}