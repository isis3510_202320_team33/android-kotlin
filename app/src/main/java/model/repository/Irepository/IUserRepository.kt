package model.repository.Irepository

import model.entity.UserEntity

interface IUserRepository {

    suspend fun authCredentials(email: String): UserEntity

    suspend fun sendDataUser(email: String, password: String): UserEntity?

    suspend fun getUser(email: String, password: String): UserEntity?

}