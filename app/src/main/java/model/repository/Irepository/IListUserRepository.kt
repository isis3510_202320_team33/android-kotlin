package model.repository.Irepository

import android.graphics.Bitmap
import com.google.firebase.firestore.DocumentSnapshot
import model.entity.UserCardEntity
import model.entity.UserEntity

interface IListUserRepository {

    suspend fun getAllUsers(pageSize: Long, lastVisibleDocument: DocumentSnapshot?): Triple<ArrayList<UserCardEntity>?, DocumentSnapshot?, String>

    fun getSizeCache(): Int

    fun getAllUserCache(): ArrayList<UserCardEntity>

    fun putUserList(key: String, value: UserCardEntity)

    fun getImage(key: String): Bitmap

    fun putImage(key: String, url: String)
}