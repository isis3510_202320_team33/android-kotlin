package utils

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import model.entity.UserEntity
import com.google.gson.Gson
object LocalStorageUser {

    fun getUserEntityFromSharedPreferences(context: Context, keyss: String): UserEntity? {
        //val sharedPreferences = context.getSharedPreferences("USER_DATA", Context.MODE_PRIVATE)
        val sharedPreferences = encryptedSharedPreferences(context, keyss)
        if (sharedPreferences == null)
        {
            Log.d("Valores", "no se obtuvo shared preferences")
            return UserEntity(full_name = "No data")
        }
        else
        {
            val json = sharedPreferences?.getString("user_data", null)
            return if (json != null) {
                val gson = Gson()
                gson.fromJson(json, UserEntity::class.java)
            } else {
               return null
            }
        }
    }

    private fun encryptedSharedPreferences(context: Context, keyss: String): SharedPreferences? {
        return try {
            val masterKey = MasterKey.Builder(context, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
                .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
                .build()
            EncryptedSharedPreferences.create(
                context,
                keyss,
                masterKey,
                EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
            )
        } catch (e: Exception) {
            Log.e("ERROR", "Error al encriptar el sharedPreferences")
            null
        }
    }


    suspend fun saveUserEntityToSharedPreferences(context: Context, userEntity: UserEntity, keyss: String): Boolean {

        return try {
            //val sharedPreferences = context.getSharedPreferences("USER_DATA", Context.MODE_PRIVATE)
            val json = userEntityToJson(userEntity)
            if (json == null)
            {
                false
            }
            else{
                val sharedPreferences = encryptedSharedPreferences(context, keyss)
                if (sharedPreferences == null)
                {
                    false
                }
                else
                {
                    val editor = sharedPreferences.edit()
                    editor.putString("user_data", json)
                    editor.apply()
                    Log.d("Valores", "USUARIO GUARDADO $json")
                    true
                }

            }

        }catch (
            e: Exception
        ){
            Log.e("ERROR", "Error al guardar los datos", e)
            false
        }

    }

     private fun userEntityToJson(userEntity: UserEntity): String? {
         return try {
             val gson = Gson()
             gson.toJson(userEntity)
         } catch (e: Exception) {
             Log.e("ERROR", "Error al convertir a Json", e)
             null
         }

    }

    fun deleteUser(context: Context, keyss: String): Boolean
    {
        val sharedPreferences = encryptedSharedPreferences(context, keyss)
        return if (sharedPreferences == null) {
            false
        } else {
            val json = sharedPreferences.edit()
            if (json != null) {
                json.remove("user_data")
                json.apply()
                true
            } else {
                false
            }
        }
    }

}