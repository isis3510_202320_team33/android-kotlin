package utils


data class PropertyApartment (

    val name: String,
    val city: String,
    val neighborhood: String,
    val address: String,
    val housingType: String, /*Apartment or House*/

    val rentPrice: String = "",
    val stratum: Int = -1,
    val area: Double = 0.0,
    val floorNumber: Int = 0,
    val apartmentFloor: Int = 0,
    val roomsNumber: Int = 0,
    val roomArea: Double = 0.0,
    val bathroomsNumber: Int = 0,
    val laundryArea: Boolean = false,
    val internet: Boolean = false,
    val tv: Boolean = false,
    val furnished: Boolean = false,
    val elevator: Boolean = false,
    val gymnasium: Boolean = false,
    val reception: Boolean = false,
    val supermarket: Boolean = false,
    val rating: Int = 0,
    val description: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
)
