package utils.Exception

class NoDataException(message: String, errorCode: Int, cause: Throwable? = null) : Exception(message, cause) {
    val customErrorCode: Int = errorCode
}

class NetworkException(message: String, cause: Throwable? = null) : Exception(message, cause)

class CustomException(message: String, details: String, cause: Throwable? = null) : Exception(message, cause) {
    val errorDetails: String = details
}