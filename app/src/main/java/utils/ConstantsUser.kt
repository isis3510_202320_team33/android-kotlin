package utils

import android.content.Context

// 'Renter', 'Landlord'
object UserType {
    const val RENTER = "Renter"
    const val LANDLORD = "Landlord"
}
//'male', 'female', ‘other’
object UserGender{
    const val MALE = "male"
    const val FEMALE = "female"
    const val OTHER = "other"
}
//'Once a week','Twice a week','Three times a week'
//It object is used for attributes bring_people and clean
object UserBringPeopleAndClean{
    const val ONCE_WEEK = "Once a week"
    const val TWICE_WEEK = "Twice a week"
    const val THREE_WEEK = "Three times a week"
}
// 'Extrovert', 'Introvert'
object UserPersonality{
    const val EXTROVERT = "Extrovert"
    const val INTROVERT = "Introvert"
}


object UserData {
    var USER_LOGIN = getUserStringValue("data_user_login")
    var USER_REGISTER = getUserStringValue("data_user_register")
}

fun getUserStringValue(key: String): String {
    val context =  ContextApplication.appContext
    val resourceId = context.resources.getIdentifier(key, "string", context.packageName)
    return if (resourceId != 0) {
        context.getString(resourceId)
    } else {
        "Valor predeterminado o manejo de error"
    }
}

object NumberPage{
    const val PAGE_SIZE = 5L
}