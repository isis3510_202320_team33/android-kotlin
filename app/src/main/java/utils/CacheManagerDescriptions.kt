package utils
import android.util.LruCache

class CacheManagerDescriptions private constructor() {

    private val maxCacheSize: Int = (Runtime.getRuntime().maxMemory() / 1024).toInt() / 8
    private val lruCache: LruCache<String, List<String>> = LruCache(maxCacheSize)

    fun saveToCache(key: String, data: List<String>) {
        lruCache.put(key, data)
    }

    fun getFromCache(key: String): List<String>? {
        return lruCache.get(key)
    }

    companion object {
        private var instance: CacheManagerDescriptions? = null

        fun getInstance(): CacheManagerDescriptions {
            if (instance == null) {
                synchronized(CacheManagerDescriptions::class) {
                    if (instance == null) {
                        instance = CacheManagerDescriptions()
                    }
                }
            }
            return instance!!
        }
    }
}