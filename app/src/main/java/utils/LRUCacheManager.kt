package utils

import android.util.LruCache
import model.entity.ApartmentEntity

class LRUCacheManager() {

    private lateinit var memoryCache: LruCache<String, ApartmentEntity>
    fun setCacheSize(cacheSize: Int) {
        memoryCache = LruCache(cacheSize)
    }

    fun size(): Int {
        return memoryCache.size()
    }

    fun put(key: String, value: ApartmentEntity){
        if (memoryCache.get(key) == null) {
            memoryCache.put(key, value)
        }
    }

    fun get(key: String): ApartmentEntity {
        return memoryCache.get(key)
    }

    fun getAll(): ArrayList<ApartmentEntity>{
        var snapshot: MutableMap<String, ApartmentEntity> = memoryCache.snapshot()
        return ArrayList(snapshot.values)
    }
}