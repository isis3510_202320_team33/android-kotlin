import android.util.LruCache
import model.HouseDTO.HouseDTO

class CacheManager private constructor() {

    private val maxCacheSize: Int = (Runtime.getRuntime().maxMemory() / 1024).toInt() / 8
    private val lruCache: LruCache<String, List<HouseDTO>> = LruCache(maxCacheSize)

    fun saveToCache(key: String, data: List<HouseDTO>) {
        lruCache.put(key, data)
    }

    fun getFromCache(key: String): List<HouseDTO>? {
        return lruCache.get(key)
    }

    companion object {
        private var instance: CacheManager? = null

        fun getInstance(): CacheManager {
            if (instance == null) {
                synchronized(CacheManager::class) {
                    if (instance == null) {
                        instance = CacheManager()
                    }
                }
            }
            return instance!!
        }
    }
}