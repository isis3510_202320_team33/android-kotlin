package utils

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.android_kotlin.R

object CachingImage
{
    fun downloadImage(imageView: ImageView, imageUrl: String) {

        Glide.with(ContextApplication.appContext)
            .load(imageUrl)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.profile_user)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.warning)
            )
            .into(imageView)
    }
}