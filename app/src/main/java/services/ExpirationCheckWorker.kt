package services

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import kotlinx.coroutines.coroutineScope
import model.repository.model.db.AppDatabase
import utils.ContextApplication

class ExpirationCheckWorker (appContext: Context, workerParams: WorkerParameters
) : CoroutineWorker(appContext, workerParams) {

    override suspend fun doWork(): Result = coroutineScope {
        try {

            val database = AppDatabase.getDatabase(ContextApplication.appContext)
            Log.d("Valores", "Conexion establecidad por el worker")
            database.userDao().deleteExpiredEntity()
            Log.d("Valores", "Usuario eliminado por el worker")
            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }
}